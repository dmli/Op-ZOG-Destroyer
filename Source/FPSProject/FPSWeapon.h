// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FPSImpactEffects.h"
#include "FPSWeapon.generated.h"

#define TRACE_WEAPON ECC_GameTraceChannel1

UENUM(BlueprintType)
namespace EWeaponProjectile
{
	enum ProjectileType
	{
		EBullet		UMETA(Displayname = "Bullet"),
		EProjectile	UMETA(Displayname = "Projectile"),
		ESpread		UMETA(Displayname = "Spread") //Shotgun ie.
	};
}

USTRUCT()
struct FWeaponData
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditDefaultsOnly, Category = Ammo)
		int32 MaxAmmo;

		UPROPERTY(EditDefaultsOnly, Category = Ammo)
		int32 AmmoPerMagazine;

		UPROPERTY(VisibleAnywhere, Category = Ammo)
		int32 CurrentAmmoInMagazine;

		UPROPERTY(EditDefaultsOnly, Category = Ammo)
		int32 InitialMagazines;

		UPROPERTY(EditDefaultsOnly, Category = Config)
		float TimeBetweenShots;

		UPROPERTY(EditDefaultsOnly, Category = Config)
		float WeaponRecoil;

		UPROPERTY(EditDefaultsOnly, Category = Config)
		float WeaponSpread;

		UPROPERTY(EditDefaultsOnly, Category = Config)
		float WeaponDamage;

		UPROPERTY(EditDefaultsOnly, Category = Config)
		float WeaponRange;

		UPROPERTY(EditDefaultsOnly, Category = Config)
		bool AllowChangeFiremode;

		/** type of damage */
		UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
		TSubclassOf<UDamageType> DamageType;
};

USTRUCT()
struct FAltWeaponData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	int32 MaxAmmo;

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	int32 AmmoPerMagazine;

	UPROPERTY(VisibleAnywhere, Category = Ammo)
	int32 CurrentAmmoInMagazine;

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	int32 InitialMagazines;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float TimeBetweenShots;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float WeaponRecoil;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float WeaponSpread;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float WeaponDamage;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float WeaponRange;
};

UCLASS()
class FPSPROJECT_API AFPSWeapon : public AActor
{
	GENERATED_BODY()



public:	
	UFUNCTION()
	void Fire();

	UFUNCTION()
	void AltFire();

	UFUNCTION()
	void Instant_Fire();

	UFUNCTION()
	void Firemode();

	UFUNCTION()
	void StopFiring();

	UFUNCTION()
	void UseAmmo();

	UFUNCTION()
	void Reload();

	UFUNCTION()
	void StopReload();

	UFUNCTION()
	void SetOwningPawn(AFPSCharacter* NewOwner);

	UFUNCTION()
	void SetOwningBot(AFPSBot* NewOwner);

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	class AFPSCharacter* GetPawnOwner() const;

	UFUNCTION()
	void AttachMeshToPawn();

	UFUNCTION()
	FVector GetAdjustedAim() const;

	UFUNCTION()
	FVector GetCameraDamageStartLocation(const FVector& AimDir) const;

	UFUNCTION()
	void StartSlowMotion();

	UFUNCTION()
	void StopSlowMotion();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Constructor for AFPSWeapon
	AFPSWeapon(const FObjectInitializer& ObjectInitializer);

	/** pawn owner */
	UPROPERTY(Transient)
	class AFPSCharacter* MyPawn;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FWeaponData WeaponConfig;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FAltWeaponData AltWeaponConfig;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Config)
	TEnumAsByte<EWeaponProjectile::ProjectileType> ProjectileType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Config)
	TEnumAsByte<EWeaponProjectile::ProjectileType> AltProjectileType;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Collision)
	UBoxComponent* CollisionComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Config)
	USkeletalMeshComponent* WeaponMesh;

	/** current total ammo */
	UPROPERTY(Transient)
	int32 CurrentAmmo;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	int32 bFiremode;

	/** current ammo - inside clip */
	UPROPERTY(Transient)
	int32 CurrentAmmoInClip;

	FTimerHandle BurstfireTimerHandle;
	FTimerHandle BurstfireTimerHandle2;

	FTimerHandle MuzzleflashTimerHandle;

	FTimerHandle ReloadTimerHandle;

	/** time of last successful weapon fire */
	float LastFireTime;

	float LastBurstFireTime;

	float LastAltSpreadFireTime;

	bool isRecoiling;

	bool bIsReloading;

	float GetRecoil();

	void Recoil();

	float FinalRecoilPitch;

	float FinalRecoilYaw;

	float CurrentRecoilPitch;

	float CurrentRecoilYaw;

	bool bAltFire;

	UFUNCTION()
	UAudioComponent* PlayWeaponSound(USoundCue* Sound);

	int32 GetCurrentAmmo() const;


	int32 GetCurrentAmmoInMagazine() const;


	int32 GetAmmoPerClip() const;


	int32 GetMaxAmmo() const;

	FName HitBoneName;

	UFUNCTION()
	FName GetHitBoneName();

	UFUNCTION()
	FVector GetMuzzleLocation() const;

protected:

	uint8 bWantsToFire : 1;

	/** is muzzle FX looped? */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	uint32 bLoopedMuzzleFX : 1;

	FHitResult WeaponTrace(const FVector &TraceFrom, const FVector &TraceTo) const;

	void ProcessHit_InstantHit(const FHitResult &Impact, const FVector &Origin, const FVector &ShootDir, int32 RandomSeed, float ReticleSpread);

	/** firing audio (bLoopedFireSound set) */
	UPROPERTY(Transient)
	UAudioComponent* FireAC;

	/** single fire sound (bLoopedFireSound not set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* FireSound;

	/** looped fire sound (bLoopedFireSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* FireLoopSound;

	/** finished burst sound (bLoopedFireSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* FireFinishSound;

	/** out of ammo sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* OutOfAmmoSound;

	/** reload sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* ReloadSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* ShotgunSound;

	/** reload animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	UAnimMontage* ReloadAnim;

	/** fire animations */
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	UAnimMontage* FireAnim;

	/** play weapon animations */
	float PlayWeaponAnimation(UAnimMontage* Animation);

	/** stop playing weapon animations */
	void StopWeaponAnimation(UAnimMontage* Animation);

	/** name of bone/socket for muzzle in weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName MuzzleAttachPoint;

	/** FX for muzzle flash */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UParticleSystem* MuzzleFX;

	/** spawned component for muzzle FX */
	UPROPERTY(Transient)
	UParticleSystemComponent* MuzzlePSC;

	/** continue processing the instant hit, as if it has been confirmed by the server */
	void ProcessInstantHit_Confirmed(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, int32 RandomSeed, float ReticleSpread);

	/** handle damage */
	void DealDamage(const FHitResult& Impact, const FVector& ShootDir);

	/** impact effects */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<class AFPSImpactEffects> ImpactTemplate;

	/** smoke trail */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UParticleSystem* TrailFX;

	/** param name for beam target in smoke trail */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName TrailTargetParam;

	/** spawn effects for impact */
	void SpawnImpactEffects(const FHitResult& Impact);

	/** spawn trail effect */
	void SpawnTrailEffect(const FVector& EndPoint);
};
