// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSProject.h"
#include "FPSCharacter.h"
#include "GameFramework/PlayerController.h"
#include "FPSProjectile.h"
#include "FPSBot.h"



AFPSCharacter::AFPSCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Create a CameraComponent 
	FirstPersonCameraComponent = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->AttachParent = GetCapsuleComponent();
	// Position the camera a bit above the eyes
	FirstPersonCameraComponent->RelativeLocation = FVector(0, 0, 76.f);
	// Allow the pawn to control rotation.
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
	FirstPersonCameraComponent->FieldOfView = 100.f;
	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	FirstPersonMesh = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("FirstPersonMesh"));
	FirstPersonMesh->SetOnlyOwnerSee(true);         // only the owning player will see this mesh
	FirstPersonMesh->AttachParent = FirstPersonCameraComponent;
	FirstPersonMesh->bCastDynamicShadow = false;
	FirstPersonMesh->CastShadow = false;
	// everyone but the owner can see the regular body mesh
	GetMesh()->SetOwnerNoSee(true);

	// Collision detection box for wallhumping.
	WallhumpBox = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("WallhumpBox"));
	WallhumpBox->InitBoxExtent(FVector(40.0f, 1.f, 1.f));
	WallhumpBox->AttachParent = GetCapsuleComponent();
	WallhumpBox->RelativeLocation = FVector(50.0f, 0.0f, 0.0f);
	WallhumpBox->bOwnerNoSee = true;
	WallhumpBox->SetCollisionObjectType(ECC_Pawn);
	WallhumpBox->OnComponentBeginOverlap.AddDynamic(this, &AFPSCharacter::OnBeginOverlapFront);
	WallhumpBox->OnComponentEndOverlap.AddDynamic(this, &AFPSCharacter::OnEndOverlapFront);

	NoiseEmitterComponent = ObjectInitializer.CreateDefaultSubobject<UPawnNoiseEmitterComponent>(this, TEXT("NoiseEmitterComponent"));

	/*
	WallrunBox = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("WallrunBox"));
	WallrunBox->InitBoxExtent(FVector(1.0f, 90.f, 1.f));
	WallrunBox->AttachParent = GetCapsuleComponent();
	WallrunBox->RelativeLocation = FVector(0.0f, 0.0f, 0.0f);
	WallrunBox->bOwnerNoSee = true;
	WallrunBox->SetCollisionObjectType(ECC_Pawn);
	WallrunBox->OnComponentBeginOverlap.AddDynamic(this, &AFPSCharacter::OnBeginOverlapSides);
	WallrunBox->OnComponentEndOverlap.AddDynamic(this, &AFPSCharacter::OnEndOverlapSides);
	*/

	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;

	TotalHeight = 86.f;
	TotalWidth = 65.f;
	CrouchHeight = 32.f;
	CrouchWidth = 32.f;
	ProneHeight = 26.f;
	ProneWidth = 26.f;

	bWantsToRun = false;
	bIsTargeting = false;
	bIsFullStamina = true;
	bCanRun = false;
	bIsRunning = false;
	bCanDive = true;
	bIsDiving = false;
	bIsProning = false;
	bAllowUnProne = false;
	bHitTheGround = false;
	bWantsToUnProne = false;
	bHasWallhumped = false;
	bIsDying = false;
	bTookDamage = false;
	//bCanWallhump = false;
	bWallhumpCount = 0;
	bMidAir = false;
	bInSlowMotion = false;

	GetCharacterMovement()->MaxWalkSpeed = 700.f;
	
	Stamina = 100.f;
	SlowMotionTime = 5.0f;
	RunningSpeedModifier = 1.5f;

	World = GetWorld();

	Score = 0;

	//NoiseEmitterComponent->NoiseLifetime = 1.0f;
}

// Sets default values
AFPSCharacter::AFPSCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	int32 NumWeaponClasses = DefaultInventoryClasses.Num();
	for (int32 i = 0; i < NumWeaponClasses; i++)
	{
		if (DefaultInventoryClasses[i])
		{
			FActorSpawnParameters SpawnInfo;
			//SpawnInfo.bNoCollisionFail = true;
			AFPSWeapon* NewWeapon = GetWorld()->SpawnActor<AFPSWeapon>(DefaultInventoryClasses[i], SpawnInfo);
			AddWeapon(NewWeapon);
		}
	}

	CurrentHealth = MaxHealth;

	GetCurrentWeapon();
	CurrentWeapon->SetOwningPawn(this);

	if (GEngine)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("We are using FPSCharacter!"));
	}
}

// Called every frame
void AFPSCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (bInSlowMotion)
	{
		//Times 2,5 because of 0,4 Time Dilution.
		SlowMotionTime -= (DeltaTime * 2.5f);
		if (SlowMotionTime <= 0)
		{
			SlowMotionTime = 0.0f;
			StopSlowMotion();
		}
	}

	if (GetVelocity() != FVector (0.0f, 0.0f, 0.0f) && !GetCharacterMovement()->IsFalling())
	{
		APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0);
		if (PC)
		{
			if (RunningCameraShake)
			{
				float ShakeAmount = FMath::Clamp(GetVelocity().Size() * 0.00005f, 0.01f, 0.08f);
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("ShakeAmount is ") + FString::SanitizeFloat(ShakeAmount));
				PC->ClientPlayCameraShake(RunningCameraShake, ShakeAmount, ECameraAnimPlaySpace::CameraLocal, FRotator(0, 0, 0));
			}
		}
	}

	if (GetCharacterMovement()->IsFalling())
	{
		bMidAir = true;
	}

	if (bMidAir && !GetCharacterMovement()->IsFalling())
	{
		APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0);
		if (PC)
		{
			if (LandCameraShake)
			{
				float FallShakeAmount = FMath::Clamp(GetVelocity().Size() * 0.001f, 0.6f, 1.5f);
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("GetVelocity().Size() is ") + FString::SanitizeFloat(GetVelocity().Size()));
				PC->ClientPlayCameraShake(LandCameraShake, FallShakeAmount, ECameraAnimPlaySpace::CameraLocal, FRotator(0, 0, 0));
			}
		}
		bMidAir = false;
	}

	if (bIsProning)
	{

		//GetCapsuleComponent()->SetCapsuleSize(ProneWidth, ProneHeight, true);
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "I'm trying to prone.");

		if (!GetCharacterMovement()->IsFalling() && !bHitTheGround) {
			bAllowUnProne = true;
			//GetWorldTimerManager().SetTimer(this, &AShooterCharacter::AllowUnProne, 1.5f, true);
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "I hit the gruond.");
			//SetAllowUnProne(true);
			bHitTheGround = true;
			bIsDiving = false;
		}

		GetCapsuleComponent()->SetCapsuleSize(ProneWidth, FMath::FInterpTo(GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), ProneHeight, DeltaTime, 30.f), true);
	}

	if (!bIsProning && bWantsToUnProne)
	{
		FVector newLoc = GetActorLocation();

		newHeight = FMath::FInterpTo(GetCapsuleComponent()->GetScaledCapsuleHalfHeight(), TotalHeight, DeltaTime, 3.f);
		newRadius = FMath::FInterpTo(GetCapsuleComponent()->GetScaledCapsuleRadius(), TotalWidth, DeltaTime, 3.f);

		if (GetCharacterMovement()->IsMovingOnGround())
		{
			newLoc.Z -= GetCapsuleComponent()->GetScaledCapsuleHalfHeight() - newHeight;
		}

		if (CanIncreaseCollision(newRadius, newHeight, newLoc))
		{
			SetActorLocation(newLoc);
			GetCapsuleComponent()->SetCapsuleSize(newRadius, newHeight, true);

			//FString TestHUDString = FString(TEXT("I want to stand up."));
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TestHUDString);
		}

		if (GetCapsuleComponent()->GetScaledCapsuleHalfHeight() > (TotalHeight * 0.99f))
		{
			bWantsToUnProne = false;
			GetCapsuleComponent()->SetCapsuleSize(TotalWidth, TotalHeight, true);
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "SetProning(false)");
			bIsProning = false;
			//SetProning(false);
		}
	}

	if (bWantsToRun && bCanRun)
	{
		if (GetVelocity() != FVector(0, 0, 0) && Stamina > 0.f)
		{
			Stamina -= 33.f * DeltaTime;
			bIsRunning = true;
			if (Stamina < 0)
			{				
				Stamina = 0.f;
			}
			//FString TheFloatStr = FString::SanitizeFloat(Stamina);
			//GEngine->AddOnScreenDebugMessage(0, 5.f, FColor::Blue, TEXT("Stamina: ") + TheFloatStr);
		}
		else
		{
			bWantsToRun = false;
			bIsRunning = false;
		}
	}

	if (!IsFullStamina() && !bIsRunning)
	{
		Stamina += 10.f * DeltaTime;
		if (Stamina > 100.f)
		{
			Stamina = 100.f;
		}
		//FString TheFloatStr = FString::SanitizeFloat(Stamina);
		//GEngine->AddOnScreenDebugMessage(0, 5.f, FColor::Blue, TEXT("Stamina: ") + TheFloatStr);
	}

	if (bIsCrouching)
	{
		FVector newLoc = GetActorLocation();

		newHeight = FMath::FInterpTo(GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight(), CrouchHeight, DeltaTime, 20.f);
		newRadius = FMath::FInterpTo(GetCapsuleComponent()->GetUnscaledCapsuleRadius(), CrouchWidth, DeltaTime, 20.f);

		if (GetCharacterMovement()->IsMovingOnGround())
		{
			newLoc.Z -= GetCapsuleComponent()->GetScaledCapsuleHalfHeight() - newHeight;
		}

		if (CanIncreaseCollision(newRadius, newHeight, newLoc))
		{
			SetActorLocation(newLoc);
			GetCapsuleComponent()->SetCapsuleSize(newRadius, newHeight, true);
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "I'm trying to crouch.");
		}

		if (GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight() < (TotalHeight / 2))
		{
			bHasCrouched = true;
			//Crouch();
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Finished crouching.");
		}
	}

	if (!bIsCrouching && bHasCrouched)
	{
		FVector newLoc = GetActorLocation();
		newHeight = FMath::FInterpTo(GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight(), TotalHeight, DeltaTime, 15.f);
		newRadius = FMath::FInterpTo(GetCapsuleComponent()->GetUnscaledCapsuleRadius(), TotalWidth, DeltaTime, 15.f);

		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "I'm trying to uncrouch.");
		if (GetCharacterMovement()->IsMovingOnGround())
		{
			newLoc.Z -= GetCapsuleComponent()->GetScaledCapsuleHalfHeight() - newHeight;
		}

		if (CanIncreaseCollision(newRadius, newHeight, newLoc))
		{
			SetActorLocation(newLoc);
			GetCapsuleComponent()->SetRelativeRotation(FRotator (0.0f, 90.0f, 0.0f));
			GetCapsuleComponent()->SetCapsuleSize(newRadius, newHeight, true);
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "I'm trying to uncrouch and set capsule size.");
		}

		if (GetCapsuleComponent()->GetScaledCapsuleHalfHeight() >= TotalHeight)
		{
			bHasCrouched = false;
			GetCapsuleComponent()->SetCapsuleSize(TotalWidth, TotalHeight, true);
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "SetCapsuleSize(TotalWidth, TotalHeight, true);");
		}
	}

	if (bHasWallhumped && !GetCharacterMovement()->IsFalling()) {
		bHasWallhumped = false;
		bWallhumpCount = 0;
	}

	/*if (bWantsToWallrun && bTouchingWall && GetCharacterMovement()->IsFalling())
	{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Wallrunning.");
			GetCharacterMovement()->AddForce(FVector(0.0f, 0.0f, 150000.0f));
			
	}*/
}

/** Take damage, handle death */
float AFPSCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	CurrentHealth -= Damage;

	PlayCharacterSound(DamageSound);
	bTookDamage = true;
	//GetWorld()->GetTimerManager().SetTimer(DeathTimerHandle, this, &AFPSCharacter::ResetTookDamage, 0.3f);

	APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0);
	if (PC)
	{
		if (DamageCameraShake)
		{
			PC->ClientPlayCameraShake(DamageCameraShake, 0.6f, ECameraAnimPlaySpace::CameraLocal, FRotator(0, 0, 0));
		}
	}
	

	if (CurrentHealth <= 0 && !bIsDying)
	{
		PlayCharacterSound(DeathSound);
		bIsDying = true;
		OnDeath();
		//Die(Damage, DamageEvent, EventInstigator, DamageCauser);
	}

	APlayerController* MyPC = GetWorld()->GetFirstPlayerController();
	AFPSHUD* MyHUD = MyPC ? Cast<AFPSHUD>(MyPC->GetHUD()) : NULL;
	if (MyHUD)
	{
		float Angle = GetActorForwardVector().CosineAngle2D(DamageCauser->GetActorLocation());
		float RightSide = GetActorRightVector().CosineAngle2D(DamageCauser->GetActorLocation());
		bool bLeftSide;

		if (RightSide <= 0)
		{
			bLeftSide = false;
		}
		else if (RightSide > 0)
		{
			bLeftSide = true;
		}

		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::SanitizeFloat(Angle));

		MyHUD->NotifyHitAngle(Angle, bLeftSide, Damage);
	}

	

	return Damage;
}

void AFPSCharacter::ResetTookDamage()
{
	bTookDamage = false;

	APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0);
	if (PC)
	{
		if (DamageCameraShake)
		{
			PC->ClientStopCameraShake(DamageCameraShake);
		}
	}
}

void AFPSCharacter::Death()
{
	TeleportTo(FVector(8713.527344f, -5489.443848f, 3130.635498f), FRotator(0.0f, -12.489219f, 130.788315f), false, false);
	FirstPersonMesh->SetOwnerNoSee(true);
	CurrentWeapon->WeaponMesh->SetOwnerNoSee(true);

	//Destroy();
}

void AFPSCharacter::OnDeath()
{
	bTearOff = true;

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);

	if (GetMesh())
	{
		static FName CollisionProfileName(TEXT("Ragdoll"));
		GetMesh()->SetCollisionProfileName(CollisionProfileName);
	}
	SetActorEnableCollision(true);


	GetMesh()->SetAllBodiesSimulatePhysics(true);
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->WakeAllRigidBodies();
	GetMesh()->bBlendPhysics = true;

	/*
	WeaponMesh->DetachFromParent();
	WeaponMesh->SetAllBodiesSimulatePhysics(true);
	WeaponMesh->SetSimulatePhysics(true);
	WeaponMesh->WakeAllRigidBodies();
	WeaponMesh->bBlendPhysics = true;
	*/
	GetCharacterMovement()->StopMovementImmediately();
	GetCharacterMovement()->DisableMovement();
	GetCharacterMovement()->SetComponentTickEnabled(false);

	//FName HitBone = SensedPawn->CurrentWeapon->GetHitBoneName();
	//GetMesh()->AddImpulseAtLocation((GetActorLocation() + SensedPawn->GetActorLocation() * 10.f), SensedPawn->GetActorLocation(), HitBone);

	//float DeathAnimDuration = PlayAnimMontage(DeathAnim);
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Dead!");
	//SetActorEnableCollision(false);

	//GetWorld()->GetTimerManager().SetTimer(DeathTimerHandle, this, &AFPSCharacter::Death, 1.0f);
	//GetWorld()->GetTimerManager().SetTimer(DeathTimerHandle2, this, &AFPSBot::SetRagdoll, FMath::Min(0.1f, DeathAnimDuration));

	Death();
}

UAudioComponent* AFPSCharacter::PlayCharacterSound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, GetCapsuleComponent());
		//FString TestHUDString = FString(TEXT("PlayCharacterSound"));
	}

	return AC;
}

// Called to bind functionality to input
void AFPSCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	// set up gameplay key bindings

	// WASD/arrow key movement
	InputComponent->BindAxis("MoveForward", this, &AFPSCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AFPSCharacter::MoveRight);

	// Mouselook
	InputComponent->BindAxis("Turn", this, &AFPSCharacter::AddControllerYawInput);
	InputComponent->BindAxis("LookUp", this, &AFPSCharacter::AddControllerPitchInput);

	// General movement actions
	InputComponent->BindAction("Jump", IE_Pressed, this, &AFPSCharacter::OnStartJump);
	InputComponent->BindAction("Jump", IE_Released, this, &AFPSCharacter::OnStopJump);
	InputComponent->BindAction("Crouch", IE_Pressed, this, &AFPSCharacter::OnStartCrouch2);
	InputComponent->BindAction("Crouch", IE_Released, this, &AFPSCharacter::OnStopCrouch2);
	InputComponent->BindAction("Sprint", IE_Pressed, this, &AFPSCharacter::OnStartSprint);
	InputComponent->BindAction("Sprint", IE_Released, this, &AFPSCharacter::OnStopSprint);

	// Stunts
	InputComponent->BindAction("Dive", IE_Pressed, this, &AFPSCharacter::OnStartDiving);
	InputComponent->BindAction("SlowMotion", IE_Pressed, this, &AFPSCharacter::SlowMotion);

	// Combat actions
	InputComponent->BindAction("Fire", IE_Pressed, this, &AFPSCharacter::OnFire);
	InputComponent->BindAction("Fire", IE_Released, this, &AFPSCharacter::OnStopFire);
	InputComponent->BindAction("AltFire", IE_Pressed, this, &AFPSCharacter::OnAltFire);

	InputComponent->BindAction("ChangeFiremode", IE_Pressed, this, &AFPSCharacter::ChangeFiremode);
	InputComponent->BindAction("Reload", IE_Pressed, this, &AFPSCharacter::OnReload);

}

void AFPSCharacter::SlowMotion()
{
	if (SlowMotionTime > 1.0f && !bInSlowMotion)
	{
		bInSlowMotion = true;
		CurrentWeapon->StartSlowMotion();
		CustomTimeDilation = 0.4f;

		PlayCharacterSound(EnterSlowMotionSound);
		PlayCharacterSound(SlowMotionSound);

		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AFPSBot::StaticClass(), FoundActors);

		for (auto It = FoundActors.CreateIterator(); It; It++)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Found Actor: ") + FoundActors.Pop()->GetName());
			//AFPSBot* Char = Cast<AFPSBot>(FoundActors.Pop());
			AFPSBot* Char = Cast<AFPSBot>((*It));
			if (Char)
			{
				if (!Char->bInSlowMotion)
				{
					Char->StartSlowMotion(SlowMotionTime);
				}			
			}
		}
	}
}

void AFPSCharacter::StopSlowMotion()
{
	CurrentWeapon->StopSlowMotion();
	bInSlowMotion = false;
	CustomTimeDilation = 1.0f;
	PlayCharacterSound(ExitSlowMotionSound);
}

/*
void AFPSCharacter::SlowMotionQuery()
{
TArray<AActor*> FoundActors;
UGameplayStatics::GetAllActorsOfClass(GetWorld(), AFPSBot::StaticClass(), FoundActors);

for (auto It = FoundActors.CreateIterator(); It; It++)
{
//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Found Actor: ") + FoundActors.Pop()->GetName());
//AFPSBot* Char = Cast<AFPSBot>(FoundActors.Pop());
AFPSBot* Char = Cast<AFPSBot>((*It));
if (Char)
{
if (!Char->bInSlowMotion)
{
Char->StartSlowMotion(SlowMotionTime);
}
}
}
}
*/
void AFPSCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		FRotator Rotation = Controller->GetControlRotation();
		// Limit pitch when walking or falling
		if (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling())
		{
			Rotation.Pitch = 0.0f;
		}
		// add movement in that direction
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
		if (bWantsToRun && (GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight() > 0.95f * TotalHeight))
		{
			if (Value > 0)
			{
				bCanRun = true;
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Value * 1.5f;"));
				GetCharacterMovement()->MaxWalkSpeed = 700.f * RunningSpeedModifier;				
			}
			else
			{
				bCanRun = false;
				bWantsToRun = false;
			}
		}
		if (!bWantsToRun)
		{
			GetCharacterMovement()->MaxWalkSpeed = 700.f;
		}
		if (bIsProning)
		{
			Value = 0.0f;
		}
		if (GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight() > CrouchHeight && GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight() < TotalHeight * 0.90f)
		{
			Value *= 0.6f;
		}
		if (GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight() == CrouchHeight)
		{
			Value *= 0.4f;
		}
		AddMovementInput(Direction, Value);
		NoiseEmitterComponent->MakeNoise(this, 1.0f, GetActorLocation());
	}
}

void AFPSCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
		// add movement in that direction
		if (bWantsToRun && bCanRun && bIsRunning) {
			Value *= 0.5f;
		}
		else if (GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight() < CrouchHeight)
		{
			Value *= 0.25f;
		}	
		else if (GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight() > CrouchHeight && GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight() < TotalHeight * 0.90f)
		{
			Value *= 0.6f;
		}
		else if (GetCapsuleComponent()->GetUnscaledCapsuleHalfHeight() == CrouchHeight)
		{
			Value *= 0.4f;
		}
		AddMovementInput(Direction, Value);
		NoiseEmitterComponent->MakeNoise(this, 1.0f, GetActorLocation());
	}
}

void AFPSCharacter::OnStartJump()
{
	APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0);

	if (bIsProning)
	{
		if (bAllowUnProne)
		{
			bIsProning = false;
			//SetUnproning(true);
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "bWantsToUnProne");
			bWantsToUnProne = true;
			World->GetTimerManager().SetTimer(ResetDiveTimerHandle, this, &AFPSCharacter::ResetDive, .5f);

			if (PC)
			{
				if (UnproneCameraShake)
				{
					PC->ClientPlayCameraShake(UnproneCameraShake, 1, ECameraAnimPlaySpace::CameraLocal, FRotator(0, 0, 0));
				}
			}
		}
		return;
	}

	/*if (bWallhumpCount == 0)
	{
		World->GetTimerManager().SetTimer(ResetDiveTimerHandle, this, &AFPSCharacter::AllowWallhump, .5f);
	}*/

	if (bTouchingWallFront && GetCharacterMovement()->IsFalling() && GetVelocity().Component(2) < 200.f && bWallhumpCount < 2)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Wallhumping.");

		LaunchCharacter(FVector(0.0f, 0.0f, 900.f), false, false);

		bHasWallhumped = true;

		//bCanWallhump = false;
		bWallhumpCount++;

		PlayCharacterSound(WallhumpSound);

		return;
	}
	/*
	if (bTouchingWall && GetCharacterMovement()->IsFalling())
	{
		bWantsToWallrun = true;
	}
	*/
	if (bHasWallhumped && !bIsProning)
	{
		PlayCharacterSound(JumpSound);
	}

	bCanDive = false;
	World->GetTimerManager().SetTimer(AllowDiveTimerHandle, this, &AFPSCharacter::ResetDive, .12f);
	bPressedJump = true;

	
	if (PC)
	{
		if (JumpCameraShake)
		{
			PC->ClientPlayCameraShake(JumpCameraShake, 1, ECameraAnimPlaySpace::CameraLocal, FRotator(0, 0, 0));
		}
	}
}

void AFPSCharacter::OnStopJump()
{
	bPressedJump = false;
	//bWantsToWallrun = false;
}

void AFPSCharacter::OnStartSprint()
{
	if (Stamina > 0) {
		bWantsToRun = true;
		if (bWantsToFire)
		{
			bWantsToFire = false;
		}
	}
}

void AFPSCharacter::OnStopSprint()
{
	bCanRun = false;
	bIsRunning = false;
	bWantsToRun = false;
}

bool AFPSCharacter::CanIncreaseCollision(float newWidth, float newHeight, FVector newLoc)
{
	// Collision params
	FCollisionQueryParams colQueryParams(NAME_None, false, this);
	FCollisionObjectQueryParams colObjQueryParams;

	// Capsule extent
	FVector extent;
	extent.X = newRadius * 0.5f;
	extent.Y = extent.X;
	extent.Z = newHeight * 0.5f;

	return !(GetWorld()->OverlapAnyTestByObjectType(newLoc, FQuat::Identity, colObjQueryParams, FCollisionShape::MakeCapsule(extent), colQueryParams));
}

void AFPSCharacter::OnStartCrouch2()
{
	if (CanCrouch() && !bIsProning)
	{
		SetCrouching(true);
		//bIsCrouching = true;
	}
}

void AFPSCharacter::OnStopCrouch2()
{
	SetCrouching(false);
}

void AFPSCharacter::SetCrouching(bool bNewCrouching)
{
	bIsCrouching = bNewCrouching;

	/*if (Role < ROLE_Authority)
	{
		ServerSetCrouching(bNewCrouching);
	}*/
}

void AFPSCharacter::OnStartDiving()
{
	if (!bIsProning)
	{
		bIsDiving = true;

		if (GetCharacterMovement()->IsMovingOnGround() && GetVelocity() != FVector::ZeroVector)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "GetCharacterMovement()->IsMovingOnGround()");
			DoDive((GetVelocity() * .7f + (GetActorUpVector() * 700.0f)));
		}
		if (GetCharacterMovement()->IsFalling() && !bCanDive)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "GetCharacterMovement()->IsFalling()");
			DoDive((FVector(0.f, 0.f, 100.f) + GetActorForwardVector() * 1000.0f));
		}
		if (GetCharacterMovement()->IsFalling() && bCanDive)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "GetCharacterMovement()->IsFalling()");
			DoDive((GetVelocity() * .7f + (GetActorUpVector() * 100.0f)));
		}
		if (GetVelocity() == FVector::ZeroVector)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "GetVelocity() == FVector::ZeroVector");
			DoDive((FVector(0.f, 0.f, 980.f) + GetActorForwardVector() * 1000.0f));
		}
	}
}

void AFPSCharacter::DoDive(FVector LaunchVelocity)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "In DoDive.");
	//SetAllowUnProne(false);
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "SetAllowUnProne(false)");
	//SetHitTheGround(false);
	bHitTheGround = false;
	bAllowUnProne = false;
	bIsProning = true;
	LaunchCharacter(LaunchVelocity, false, false);
	bCanDive = false;
	//GetWorldTimerManager().SetTimer(SampleTimerHandle, this, &AFPSCharacter::ResetDive, 2.5f, true);
	//GetWorldTimerManager().SetTimer(this, &AFPSCharacter::AllowUnProne, 1.5f, true);
}

void AFPSCharacter::ResetDive()
{
	bCanDive = true;
}

// Overlapping functions

void AFPSCharacter::OnBeginOverlapFront(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	bTouchingWallFront = true;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "I am touching a wall front.");
	if (GetCharacterMovement()->IsFalling() && bWallhumpCount <= 2)
	{
		//bCanWallhump = true;
	}
}

void AFPSCharacter::OnEndOverlapFront(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	bTouchingWallFront = false;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "I am no longer touching a wall front.");
	//bCanWallhump = false;
}

/*
void AFPSCharacter::OnBeginOverlapSides(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "I am touching a wall (sides).");
	bTouchingWall = true;
}

void AFPSCharacter::OnEndOverlapSides(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "I am no longer touching a wall (sides).");
	bTouchingWall = false;
}
*/

// COMBAT ACTIONS

void AFPSCharacter::OnFire()
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Pressed fire!"));
	// try and fire a projectile
	/*
	if (ProjectileClass != NULL)
	{
		// Get the camera transform
		FVector CameraLoc;
		FRotator CameraRot;
		GetActorEyesViewPoint(CameraLoc, CameraRot);
		// MuzzleOffset is in camera space, so transform it to world space before offsetting from the camera to find the final muzzle position
		FVector const MuzzleLocation = CameraLoc + FTransform(CameraRot).TransformVector(MuzzleOffset);
		FRotator MuzzleRotation = CameraRot;
		MuzzleRotation.Pitch += 5.0f;          // skew the aim upwards a bit
		UWorld* const World = GetWorld();
		if (World)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;
			// spawn the projectile at the muzzle
			AFPSProjectile* const Projectile = World->SpawnActor<AFPSProjectile>(ProjectileClass, MuzzleLocation, MuzzleRotation, SpawnParams);
			if (Projectile)
			{
				// find launch direction
				FVector const LaunchDir = MuzzleRotation.Vector();
				Projectile->InitVelocity(LaunchDir);
			}
		}
	}
	*/
	if (!bWantsToFire && !bIsDying)
	{
		bWantsToFire = true;
		if (CurrentWeapon)
		{
			CurrentWeapon->Fire();
			NoiseEmitterComponent->MakeNoise(this, 1.0f, GetActorLocation());
		}
	}
}

void AFPSCharacter::OnStopFire()
{
	if (bWantsToFire)
	{
		bWantsToFire = false;
		CurrentWeapon->StopFiring();
	}
}

void AFPSCharacter::OnAltFire()
{
	if (CurrentWeapon && !bIsDying)
	{
		CurrentWeapon->AltFire();
		NoiseEmitterComponent->MakeNoise(this, 1.0f, GetActorLocation());
	}
}

void AFPSCharacter::ChangeFiremode()
{
	if (CurrentWeapon != NULL)
	{
		CurrentWeapon->Firemode();
	}	
}

void AFPSCharacter::OnReload()
{
	if (CurrentWeapon != NULL)
	{
		CurrentWeapon->Reload();
	}
}

// Getters

float AFPSCharacter::GetRunningSpeedModifier() 
{
	return RunningSpeedModifier;
}

bool AFPSCharacter::IsTargeting() const 
{
	return bIsTargeting;
}

bool AFPSCharacter::IsRunning() const
{
	if (!GetCharacterMovement())
	{
		return false;
	}

	return (bWantsToRun && !GetVelocity().IsZero() && (GetVelocity().GetSafeNormal2D() | GetActorForwardVector()) > -0.1f);
}

bool AFPSCharacter::IsFullStamina()
{
	if (Stamina == 100.f)
	{
		bIsFullStamina = true;
		return bIsFullStamina;
	}
	else
	{
		bIsFullStamina = false;
		return bIsFullStamina;
	}
}

FRotator AFPSCharacter::GetAimOffsets() const
{
	const FVector AimDirWS = GetBaseAimRotation().Vector();
	const FVector AimDirLS = ActorToWorld().InverseTransformVectorNoScale(AimDirWS);
	const FRotator AimRotLS = AimDirLS.Rotation();

	return AimRotLS;
}                       

AFPSWeapon* AFPSCharacter::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

void AFPSCharacter::SetCurrentWeapon(class AFPSWeapon* NewWeapon, class AFPSWeapon* LastWeapon)
{
	AFPSWeapon* LocalLastWeapon = NULL;

	if (LastWeapon != NULL)
	{
		LocalLastWeapon = LastWeapon;
	}
	else if (NewWeapon != CurrentWeapon)
	{
		LocalLastWeapon = CurrentWeapon;
	}

	// unequip previous
	if (LocalLastWeapon)
	{
		//LocalLastWeapon->OnUnEquip();
	}

	CurrentWeapon = NewWeapon;

	// equip new one
	if (NewWeapon)
	{
		//NewWeapon->SetOwningPawn(this);	// Make sure weapon's MyPawn is pointing back to us. During replication, we can't guarantee APawn::CurrentWeapon will rep after AWeapon::MyPawn!
		//NewWeapon->OnEquip();
	}
}

void AFPSCharacter::AddWeapon(AFPSWeapon* Weapon)
{
	if (Weapon && Role == ROLE_Authority)
	{
		//Weapon->OnEnterInventory(this);
		Inventory.AddUnique(Weapon);
		CurrentWeapon = Weapon;
	}
}

float AFPSCharacter::PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName)
{
	if (AnimMontage && FirstPersonMesh && FirstPersonMesh->AnimScriptInstance)
	{
		return FirstPersonMesh->AnimScriptInstance->Montage_Play(AnimMontage, InPlayRate);
	}

	return 0.0f;
}

void AFPSCharacter::StopAnimMontage(class UAnimMontage* AnimMontage)
{
	if (AnimMontage && FirstPersonMesh && FirstPersonMesh->AnimScriptInstance &&
		FirstPersonMesh->AnimScriptInstance->Montage_IsPlaying(AnimMontage))
	{
		FirstPersonMesh->AnimScriptInstance->Montage_Stop(AnimMontage->BlendOutTime);
	}
}

void AFPSCharacter::StopAllAnimMontages()
{
	if (FirstPersonMesh && FirstPersonMesh->AnimScriptInstance)
	{
		FirstPersonMesh->AnimScriptInstance->Montage_Stop(0.0f);
	}
}

FName AFPSCharacter::GetWeaponAttachPoint() const
{
	return WeaponAttachPoint;
}

USkeletalMeshComponent* AFPSCharacter::GetSpecifcPawnMesh() const
{
	return FirstPersonMesh;
}

bool AFPSCharacter::IsAlive()
{
	return true;
}

float AFPSCharacter::GetCurrentHealth()
{
	return CurrentHealth;
}

float AFPSCharacter::GetCurrentStamina()
{
	return Stamina;
}

float AFPSCharacter::GetCurrentSlowMotionTime()
{
	return SlowMotionTime;
}

int32 AFPSCharacter::GetCurrentAmmoInMagazine() const
{
	return CurrentWeapon->GetCurrentAmmoInMagazine();
}

void AFPSCharacter::AddScore()
{
	Score += 100;
	if (!bInSlowMotion)
	{
	if (SlowMotionTime < SlowMotionTimeMAX)
	{
		SlowMotionTime += 0.5f;
	}
	if (SlowMotionTime > SlowMotionTimeMAX)
	{
		SlowMotionTime = SlowMotionTimeMAX;
	}
	}
}

int32 AFPSCharacter::GetScore()
{
	return Score;
}