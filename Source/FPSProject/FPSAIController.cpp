// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSProject.h"
#include "FPSBot.h"
#include "FPSAIController.h"

AFPSAIController::AFPSAIController()
{
	//Initialize our components
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
	BehaviorTreeComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	bAllowStrafe = true;
}

void AFPSAIController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);

	//If our character is valid and has a valid Behavior Tree,
	//Initialize the values of the Blackboard and start the tree
	
	AFPSBot* Char = Cast<AFPSBot>(InPawn);
	if (Char && Char->BehaviorTree->BlackboardAsset)
	{
		//Initialize the blackboard values
		BlackboardComp->InitializeBlackboard(*Char->BehaviorTree->BlackboardAsset);

		//Start the tree
		BehaviorTreeComp->StartTree(*Char->BehaviorTree);
	}
}

void AFPSAIController::SetSensedTarget(APawn* NewTarget)
{
	//Set a new target to follow
	if (BlackboardComp)
	{
		BlackboardComp->SetValueAsObject(TargetKey, NewTarget);
		SetFocus(NewTarget);
	}
}

void AFPSAIController::SetMoveToTarget(APawn* Pawn)
{
	if (BlackboardComp)
	{
		SetSensedTarget(Pawn);

		if (Pawn)
		{
			//MoveToActor(Pawn);
			BlackboardComp->SetValueAsVector(TargetLocationKeyName, Pawn->GetActorLocation());
		}
	}
}

void AFPSAIController::FindPointNearEnemy(APawn* Pawn)
{
	/*AFPSAIController* MyController = Cast<AFPSAIController>(GetOwner());
	if (MyController == NULL)
	{
		return;
	}*/

	APawn* MyBot = GetPawn();
	AFPSCharacter* Enemy = GetEnemy();
	if (Enemy && MyBot)
	{
		const float SearchRadius = 200.0f;
		const FVector SearchOrigin = Enemy->GetActorLocation() + 600.0f * (MyBot->GetActorLocation() - Enemy->GetActorLocation()).GetSafeNormal();
		const FVector Loc = UNavigationSystem::GetRandomPointInRadius(MyBot, SearchOrigin, SearchRadius);
		if (Loc != FVector::ZeroVector)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("BlackboardComp->SetValueAsVector(TargetStrafeLocationKeyName, Loc);"));
			MoveToLocation(Loc);
			BlackboardComp->SetValueAsVector(TargetStrafeLocationKeyName, Loc);
		}
	}
}

void AFPSAIController::UpdateControlRotation(float DeltaTime, bool bUpdatePawn)
{
	// Look toward focus
	FVector FocalPoint = GetFocalPoint();
	if (!FocalPoint.IsZero() && GetPawn())
	{
		FVector Direction = FocalPoint - GetPawn()->GetActorLocation();
		FRotator NewControlRotation = Direction.Rotation();

		NewControlRotation.Yaw = FRotator::ClampAxis(NewControlRotation.Yaw);

		SetControlRotation(NewControlRotation);

		APawn* const P = GetPawn();
		if (P && bUpdatePawn)
		{
			P->FaceRotation(NewControlRotation, DeltaTime);
		}

	}
}

void AFPSAIController::ShootEnemy()
{
	/*
	AFPSBot* MyBot = Cast<AFPSBot>(GetPawn());
	AFPSWeapon* MyWeapon = MyBot ? MyBot->GetCurrentWeapon() : NULL;
	if (MyWeapon == NULL)
	{
		return;
	}

	bool bCanShoot = false;
	AFPSCharacter* Enemy = GetEnemy();
	if (Enemy && (Enemy->IsAlive()) && (MyWeapon->GetCurrentAmmo() > 0) && (MyWeapon->CanFire() == true))
	{
		if (LineOfSightTo(Enemy, MyBot->GetActorLocation()))
		{
			bCanShoot = true;
		}
	}

	if (bCanShoot)
	{
		MyBot->StartWeaponFire();
	}
	else
	{
		MyBot->StopWeaponFire();
	}*/
}

class AFPSCharacter* AFPSAIController::GetEnemy() const
{
	if (BlackboardComp)
	{
		return Cast<AFPSCharacter>(BlackboardComp->GetValueAsObject(TargetKey));
	}

	return NULL;
}

