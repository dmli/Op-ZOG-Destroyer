// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "FPSHUD.generated.h"

/**
 * 
 */

namespace EFPSHudPosition
{
	enum Type
	{
		Left = 0,
		FrontLeft = 1,
		Front = 2,
		FrontRight = 3,
		Right = 4,
		BackRight = 5,
		Back = 6,
		BackLeft = 7,
	};
}

UCLASS()
class FPSPROJECT_API AFPSHUD : public AHUD
{
	GENERATED_BODY()
	
		AFPSHUD(const FObjectInitializer& ObjectInitializer);

	public:

	UFUNCTION(BlueprintCallable, Category = "HUD")
	void NotifyHitAngle(float Angle, bool bLeftSide, float Damage);

	private:
	/** Crosshair asset pointer */
	UTexture2D* CrosshairTex;
	
	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

	/** Crosshair icons (left, top, right, bottom and center). */
	UPROPERTY()
	FCanvasIcon Crosshair[5];

	/** Offsets to display hit indicator parts. */
	FVector2D Offsets[8];

	/** Texture for hit indicator. */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
	UTexture2D* HitIndicatorUp;

	UPROPERTY(EditDefaultsOnly, Category = HUD)
	UTexture2D* HitIndicatorDown;

	UPROPERTY(EditDefaultsOnly, Category = HUD)
	UTexture2D* HitIndicatorLeft;

	UPROPERTY(EditDefaultsOnly, Category = HUD)
	UTexture2D* HitIndicatorRight;

	/** texture for HUD elements. */
	UPROPERTY()
	UTexture2D* HUDMainTexture;

	/** Texture for HUD elements. */
	UPROPERTY()
	UTexture2D* HUDAssets02Texture;

	/** Overlay shown when health is low. */
	UPROPERTY(EditDefaultsOnly, Category = HUD)
	UTexture2D* LowHealthOverlayTexture;

	/** Health bar background icon. */
	UPROPERTY()
	FCanvasIcon HealthBarBg;

	/** Health bar icon. */
	UPROPERTY()
	FCanvasIcon HealthBar;

	/** Health icon on the health bar. */
	UPROPERTY()
	FCanvasIcon HealthIcon;

	/** Large font - used for ammo display etc. */
	UPROPERTY()
	UFont* BigFont;

	/** UI scaling factor for other resolutions than Full HD. */
	float ScaleUI;

	/** General offset for HUD elements. */
	float Offset;

	/** Most recent hit time, used to check if we need to draw hit indicator at all. */
	float LastHitTime;

	/** How long till hit notify fades out completely. */
	float HitNotifyDisplayTime;

	float LastDamageTaken;

	float LastDamageAngle;

	bool bHUDLeftSide;

	UFUNCTION(BlueprintCallable, Category = "HUD")
	float GetHealth();

	UFUNCTION(BlueprintCallable, Category = "HUD")
	float GetStamina();

	UPROPERTY()
	bool bHasTakenDamage;

	void DrawHitIndicator();

	/** Icons for hit indicator. */
	UPROPERTY()
	FCanvasIcon HitNotifyIcon[8];
};
