// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSProject.h"
#include "FPSWeapon.h"
#include "FPSCharacter.h"
#include "FPSBot.h"

// Sets default values
AFPSWeapon::AFPSWeapon(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	CollisionComp = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("CollisionComp"));
	RootComponent = CollisionComp;

	WeaponMesh = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("WeaponMesh"));
	WeaponMesh->AttachTo(RootComponent);
	WeaponMesh->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	WeaponMesh->bReceivesDecals = false;
	WeaponMesh->CastShadow = false;
	WeaponMesh->SetCollisionObjectType(ECC_WorldDynamic);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetCollisionResponseToAllChannels(ECR_Ignore);

	PrimaryActorTick.bCanEverTick = true;

	bFiremode = 0;

	isRecoiling = false;
	FinalRecoilPitch = .0f;
	FinalRecoilYaw = .0f;
	bLoopedMuzzleFX = false;
	bIsReloading = false;

	LastBurstFireTime = 1.0f;
	LastAltSpreadFireTime = 1.0f;
}

// Called when the game starts or when spawned
void AFPSWeapon::BeginPlay()
{
	Super::BeginPlay();

	WeaponConfig.CurrentAmmoInMagazine = WeaponConfig.AmmoPerMagazine;
	

	
}

// Called every frame
void AFPSWeapon::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (isRecoiling)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "isRecoiling");
		CurrentRecoilPitch = FMath::FInterpTo(CurrentRecoilPitch, FinalRecoilPitch, DeltaTime, 55.0f);
		CurrentRecoilYaw = FMath::FInterpTo(CurrentRecoilYaw, FinalRecoilYaw, DeltaTime, 55.0f);
		
		MyPawn->APawn::AddControllerPitchInput(CurrentRecoilPitch);
		MyPawn->APawn::AddControllerYawInput(CurrentRecoilYaw);

		if (CurrentRecoilPitch == FinalRecoilPitch && CurrentRecoilYaw == FinalRecoilYaw) {
			isRecoiling = false;
			CurrentRecoilPitch = 0.0f;
			CurrentRecoilYaw = 0.0f;
			FinalRecoilPitch = 0.0f;
			FinalRecoilYaw = 0.0f;
		}
	}

	if (bWantsToFire)
	{
		const float GameTime = GetWorld()->GetTimeSeconds();
		if (LastFireTime > 0 && WeaponConfig.TimeBetweenShots > 0.0f &&
			LastFireTime + WeaponConfig.TimeBetweenShots < GameTime)
		{
			Instant_Fire();
		}	
	}
}

void AFPSWeapon::SetOwningPawn(AFPSCharacter* NewOwner)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "SetOwningPawn(AFPSCharacter* NewOwner)");
	if (MyPawn != NewOwner)
	{
		

		Instigator = NewOwner;
		MyPawn = NewOwner;
		// net owner for RPC calls
		SetOwner(NewOwner);

		AttachMeshToPawn();
	}
}

void AFPSWeapon::SetOwningBot(AFPSBot* NewOwner)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "SetOwningPawn(AFPSCharacter* NewOwner)");
	//if (MyPawn != NewOwner)
	//{


		Instigator = NewOwner;
		//MyPawn = NewOwner;
		// net owner for RPC calls
		SetOwner(NewOwner);

		//AttachMeshToPawn();
	//}
}

class AFPSCharacter* AFPSWeapon::GetPawnOwner() const
{
	return MyPawn;
}

void AFPSWeapon::StartSlowMotion()
{
	CustomTimeDilation = 0.4f;
}

void AFPSWeapon::StopSlowMotion()
{
	CustomTimeDilation = 1.0f;
}

void AFPSWeapon::AttachMeshToPawn()
{
	if (MyPawn)
	{
		// Remove and hide both first and third person meshes
		//DetachMeshFromPawn();

		// For locally controller players we attach both weapons and let the bOnlyOwnerSee, bOwnerNoSee flags deal with visibility.
		FName AttachPoint = MyPawn->GetWeaponAttachPoint();
		if (MyPawn->IsLocallyControlled() == true)
		{
			USkeletalMeshComponent* PawnMesh1p = MyPawn->GetSpecifcPawnMesh();
			WeaponMesh->SetHiddenInGame(false);
			WeaponMesh->AttachTo(PawnMesh1p, AttachPoint); 
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "WeaponMesh->AttachTo(PawnMesh1p, AttachPoint);");
		}
		/*
		else
		{
			USkeletalMeshComponent* UseWeaponMesh = GetWeaponMesh();
			USkeletalMeshComponent* UsePawnMesh = MyPawn->GetPawnMesh();
			UseWeaponMesh->AttachTo(UsePawnMesh, AttachPoint);
			UseWeaponMesh->SetHiddenInGame(false);
		}*/
	}
}

void AFPSWeapon::Recoil()
{
	float RecoilValue = GetRecoil();
	FinalRecoilPitch = RecoilValue * FMath::FRandRange(-0.045f, -0.075f);
	FinalRecoilYaw = RecoilValue * FMath::FRandRange(-0.025f, 0.025f);

	isRecoiling = true;
}

void AFPSWeapon::StopFiring()
{
	bWantsToFire = false;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "StopFiring();");

	if (MuzzlePSC != NULL)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "MuzzlePSC->DeactivateSystem();");
		MuzzlePSC->DeactivateSystem();
		MuzzlePSC = NULL;
	}
	

	/*
	if (bLoopedFireAnim && bPlayingFireAnim)
	{
		StopWeaponAnimation(FireAnim);
		bPlayingFireAnim = false;
	}*/

	if (FireAC)
	{
		FireAC->FadeOut(0.1f, 0.0f);
		FireAC = NULL;

		PlayWeaponSound(FireFinishSound);
	}

	StopWeaponAnimation(FireAnim);
}

float AFPSWeapon::GetRecoil()
{
	float CurrentRecoil = WeaponConfig.WeaponRecoil;
	return CurrentRecoil;
}

void AFPSWeapon::Fire()
{
	if (ProjectileType == EWeaponProjectile::EBullet)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "EBullet");
		if (bFiremode == 0)
		{
			bWantsToFire = true;
			Instant_Fire();
			//bLoopedMuzzleFX = true;
		}
		else if (bFiremode == 1)
		{
			const float GameTime = GetWorld()->GetTimeSeconds();
			if (LastBurstFireTime > 0 && LastBurstFireTime + 0.5f < GameTime)
			{
				Instant_Fire();
				GetWorldTimerManager().SetTimer(BurstfireTimerHandle, this, &AFPSWeapon::Instant_Fire, WeaponConfig.TimeBetweenShots, false);
				GetWorldTimerManager().SetTimer(BurstfireTimerHandle2, this, &AFPSWeapon::Instant_Fire, WeaponConfig.TimeBetweenShots * 2, false);
				GetWorldTimerManager().SetTimer(MuzzleflashTimerHandle, this, &AFPSWeapon::StopFiring, WeaponConfig.TimeBetweenShots * 2.3f, false);
				LastBurstFireTime = GetWorld()->GetTimeSeconds();
			}
		}
		else if (bFiremode == 2)
		{
			Instant_Fire();
		}
	}
	if (ProjectileType == EWeaponProjectile::ESpread)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "ESpread");
		for (int32 i = 0; i <= WeaponConfig.WeaponSpread; i++)
		{
			Instant_Fire();
		}
		GetWorldTimerManager().SetTimer(MuzzleflashTimerHandle, this, &AFPSWeapon::StopFiring, WeaponConfig.TimeBetweenShots * 2, false);
	}
	if (ProjectileType == EWeaponProjectile::EProjectile)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "EProjectile");
	}
}

void AFPSWeapon::AltFire()
{
	bAltFire = true;
	if (AltProjectileType == EWeaponProjectile::EBullet)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "EBullet");
		if (bFiremode == 0)
		{
			bWantsToFire = true;
			Instant_Fire();
			//bLoopedMuzzleFX = true;
		}
		else if (bFiremode == 1)
		{
			Instant_Fire();
			GetWorldTimerManager().SetTimer(BurstfireTimerHandle, this, &AFPSWeapon::Instant_Fire, AltWeaponConfig.TimeBetweenShots, false);
			GetWorldTimerManager().SetTimer(BurstfireTimerHandle2, this, &AFPSWeapon::Instant_Fire, AltWeaponConfig.TimeBetweenShots * 2, false);
			GetWorldTimerManager().SetTimer(MuzzleflashTimerHandle, this, &AFPSWeapon::StopFiring, AltWeaponConfig.TimeBetweenShots * 2.3f, false);
		}
		else if (bFiremode == 2)
		{
			Instant_Fire();
		}
	}
	if (AltProjectileType == EWeaponProjectile::ESpread)
	{
		const float GameTime = GetWorld()->GetTimeSeconds();
		if (LastAltSpreadFireTime > 0 && LastAltSpreadFireTime + AltWeaponConfig.TimeBetweenShots < GameTime)
		{		
			PlayWeaponSound(ShotgunSound);
			for (int32 i = 0; i <= AltWeaponConfig.WeaponSpread; i++)
			{
				bAltFire = true;
				Instant_Fire();
			}
			GetWorldTimerManager().SetTimer(MuzzleflashTimerHandle, this, &AFPSWeapon::StopFiring, 0.1f, false);
			LastAltSpreadFireTime = GetWorld()->GetTimeSeconds();
		}
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "ESpread");
	}
	if (AltProjectileType == EWeaponProjectile::EProjectile)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "EProjectile");
	}
}

void AFPSWeapon::Instant_Fire()
{
	if (WeaponConfig.CurrentAmmoInMagazine == 0 && !bAltFire)
	{
		PlayWeaponSound(OutOfAmmoSound);
		StopFiring();
		return;
	}

	if (bIsReloading)
	{
		return;
	}

	PlayWeaponAnimation(FireAnim);

	if (!bAltFire)
	{
		PlayWeaponSound(FireSound);
	}
	

	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);
	const float CurrentSpread = bAltFire ? AltWeaponConfig.WeaponSpread : WeaponConfig.WeaponSpread;
	const float SpreadCone = bAltFire ? FMath::DegreesToRadians(AltWeaponConfig.WeaponSpread * 0.5f) : FMath::DegreesToRadians(WeaponConfig.WeaponSpread * 0.5f);

	const FVector AimDir = GetAdjustedAim();
	const FVector StartTrace = GetCameraDamageStartLocation(AimDir);
	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, SpreadCone, SpreadCone);
	const FVector EndTrace = bAltFire ? StartTrace + ShootDir * AltWeaponConfig.WeaponRange : StartTrace + ShootDir * WeaponConfig.WeaponRange;
	const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);

	//CurrentFiringSpread = FMath::Min(WeaponConfig.FiringSpreadMax, CurrentFiringSpread + WeaponConfig.FiringSpreadIncrement);
	ProcessHit_InstantHit(Impact, StartTrace, ShootDir, RandomSeed, CurrentSpread);

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Hitscan fire");
	if (MyPawn)
	{
		Recoil();
	}	

	if (bWantsToFire) {
		LastFireTime = GetWorld()->GetTimeSeconds();
	}

	if (MuzzleFX && MuzzlePSC == NULL)
	{
		if (MuzzlePSC == NULL)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "MuzzlePSC == NULL)");
			WeaponMesh->GetSocketLocation(MuzzleAttachPoint);
			MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, WeaponMesh, MuzzleAttachPoint);
			MuzzlePSC->ActivateSystem(true);
		}
	}

	if (Impact.bBlockingHit)
	{
		SpawnImpactEffects(Impact);
		SpawnTrailEffect(Impact.ImpactPoint);
	}
	else
	{
		SpawnTrailEffect(EndTrace);
	}

	if (!bAltFire)
	{
		UseAmmo();
	}

	bAltFire = false;
}

void AFPSWeapon::UseAmmo()
{
	WeaponConfig.CurrentAmmoInMagazine--;
}

void AFPSWeapon::Reload()
{
	if (WeaponConfig.CurrentAmmoInMagazine == 30)
	{
		return;
	}

	float AnimDuration = PlayWeaponAnimation(ReloadAnim);
	WeaponConfig.CurrentAmmoInMagazine = 30;

	bIsReloading = true;

	GetWorldTimerManager().SetTimer(ReloadTimerHandle, this, &AFPSWeapon::StopReload, AnimDuration, false);
}

void AFPSWeapon::StopReload()
{
	bIsReloading = false;
	StopWeaponAnimation(ReloadAnim);
}

FHitResult AFPSWeapon::WeaponTrace(const FVector &TraceFrom, const FVector &TraceTo) const
{
	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));

	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;
	//TraceParams.AddIgnoredActor(this);

	FHitResult Hit(ForceInit);

	GetWorld()->LineTraceSingleByChannel(Hit, TraceFrom, TraceTo, TRACE_WEAPON, TraceParams);

	return Hit;
}

void AFPSWeapon::ProcessHit_InstantHit(const FHitResult &Impact, const FVector &Origin, const FVector &ShootDir, int32 RandomSeed, float ReticleSpread)
{
	const FVector EndTrace = Origin + ShootDir * WeaponConfig.WeaponRange;
	const FVector EndPoint = Impact.GetActor() ? Impact.ImpactPoint : EndTrace;

	//DrawDebugLine(this->GetWorld(), Origin, Impact.TraceEnd, FColor::Red, true, 10000, 10.f);

	if (Impact.GetActor())
	{
		//FString message = TEXT("Hit Actor ") + Impact.GetActor()->GetName();
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);
		SpawnImpactEffects(Impact);
		DealDamage(Impact, ShootDir);
		// notify the server of the hit
		//ServerNotifyHit(Impact, ShootDir, RandomSeed, ReticleSpread);
	}
	else if (Impact.GetActor() == NULL)
	{
		if (Impact.bBlockingHit)
		{
			SpawnImpactEffects(Impact);
			SpawnTrailEffect(Impact.ImpactPoint);
			// notify the server of the hit
			//ServerNotifyHit(Impact, ShootDir, RandomSeed, ReticleSpread);
		}
		else
		{
			SpawnTrailEffect(EndTrace);
			// notify server of the miss
			//ServerNotifyMiss(ShootDir, RandomSeed, ReticleSpread);
		}
	}
		
	// process a confirmed hit
	//ProcessInstantHit_Confirmed(Impact, Origin, ShootDir, RandomSeed, ReticleSpread);	
}

void AFPSWeapon::ProcessInstantHit_Confirmed(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, int32 RandomSeed, float ReticleSpread)
{
	// handle damage
	if (Impact.GetActor())
	{
		
	}

	// play FX locally
		/*
	if (GetNetMode() != NM_DedicatedServer)
	{
		const FVector EndTrace = Origin + ShootDir * WeaponConfig.WeaponRange;
		const FVector EndPoint = Impact.GetActor() ? Impact.ImpactPoint : EndTrace;

		//SpawnTrailEffect(EndPoint);
		//SpawnImpactEffects(Impact);
	}*/
}

void AFPSWeapon::DealDamage(const FHitResult& Impact, const FVector& ShootDir)
{
	FPointDamageEvent PointDmg;
	PointDmg.DamageTypeClass = WeaponConfig.DamageType;
	PointDmg.HitInfo = Impact;
	PointDmg.ShotDirection = ShootDir;
	PointDmg.Damage = bAltFire ? AltWeaponConfig.WeaponDamage : WeaponConfig.WeaponDamage;

	FName b_head;

	if (!Impact.BoneName.Compare(b_head))
	{
		PointDmg.Damage = PointDmg.Damage * 1.5f;
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Headshot!");
	}

	Impact.GetActor()->TakeDamage(PointDmg.Damage, PointDmg, MyPawn->Controller, this);

	AFPSBot* Char = Cast<AFPSBot>(Impact.GetActor());
	if (Char)
	{
		Char->HitBone(Impact.BoneName, Impact.ImpactPoint);
	}	
}

FName AFPSWeapon::GetHitBoneName()
{
	return HitBoneName;
}

void AFPSWeapon::Firemode()
{
	if (WeaponConfig.AllowChangeFiremode) {
		bFiremode++;
		if (bFiremode == 3)
		{
			bFiremode = 0;
		}
		if (bFiremode == 2)
		{
			FString TestHUDString = FString(TEXT("Semi-auto"));
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TestHUDString);
		}
		if (bFiremode == 0)
		{
			FString TestHUDString = FString(TEXT("Full-auto"));
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TestHUDString);
		}
		else if (bFiremode == 1)
		{
			FString TestHUDString = FString(TEXT("Burst fire"));
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TestHUDString);
		}
	}
	else return;
}

FVector AFPSWeapon::GetAdjustedAim() const
{
	APlayerController* const PlayerController = Instigator ? Cast<APlayerController>(Instigator->Controller) : NULL;
	FVector FinalAim = FVector::ZeroVector;
	// If we have a player controller use it for the aim

	if (PlayerController)
	{
		FVector CamLoc;
		FRotator CamRot;
		//PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
		GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(CamLoc, CamRot);
		FinalAim = CamRot.Vector();
	}
	else if (Instigator)
	{
		// Now see if we have an AI controller - we will want to get the aim from there if we do
		AFPSAIController* AIController = MyPawn ? Cast<AFPSAIController>(MyPawn->Controller) : NULL;
		if (AIController != NULL)
		{
			FinalAim = AIController->GetControlRotation().Vector();
		}
		else
		{
			FinalAim = Instigator->GetBaseAimRotation().Vector();
		}
	}

	return FinalAim;
}

FVector AFPSWeapon::GetCameraDamageStartLocation(const FVector& AimDir) const
{
	APlayerController* PC = MyPawn ? Cast<APlayerController>(MyPawn->Controller) : NULL;
	AFPSAIController* AIPC = MyPawn ? Cast<AFPSAIController>(MyPawn->Controller) : NULL;
	FVector OutStartTrace = FVector::ZeroVector;

	if (PC)
	{
		// use player's camera
		FRotator UnusedRot;
		PC->GetPlayerViewPoint(OutStartTrace, UnusedRot);

		// Adjust trace so there is nothing blocking the ray between the camera and the pawn, and calculate distance from adjusted start
		OutStartTrace = OutStartTrace + AimDir * ((Instigator->GetActorLocation() - OutStartTrace) | AimDir);
	}
	else if (AIPC)
	{
		OutStartTrace = GetMuzzleLocation();
	}

	return OutStartTrace;
}

UAudioComponent* AFPSWeapon::PlayWeaponSound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound && MyPawn)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, MyPawn->GetRootComponent());
		//FString TestHUDString = FString(TEXT("PlayWeaponSound"));
	}

	return AC;
}

void AFPSWeapon::SpawnImpactEffects(const FHitResult& Impact)
{
	if (ImpactTemplate && Impact.bBlockingHit)
	{
		FHitResult UseImpact = Impact;

		// trace again to find component lost during replication
		if (!Impact.Component.IsValid())
		{
			const FVector StartTrace = Impact.ImpactPoint + Impact.ImpactNormal * 10.0f;
			const FVector EndTrace = Impact.ImpactPoint - Impact.ImpactNormal * 10.0f;
			FHitResult Hit = WeaponTrace(StartTrace, EndTrace);
			UseImpact = Hit;
		}

		AFPSImpactEffects* EffectActor = GetWorld()->SpawnActorDeferred<AFPSImpactEffects>(ImpactTemplate, Impact.ImpactPoint, Impact.ImpactNormal.Rotation());
		if (EffectActor)
		{
			FString TestHUDString = FString(TEXT("EffectActor->SurfaceHit = UseImpact;"));
			EffectActor->SurfaceHit = UseImpact;
			UGameplayStatics::FinishSpawningActor(EffectActor, FTransform(Impact.ImpactNormal.Rotation(), Impact.ImpactPoint));
		}
	}
}

void AFPSWeapon::SpawnTrailEffect(const FVector& EndPoint)
{
	if (TrailFX)
	{
		const FVector Origin = WeaponMesh->GetSocketLocation(MuzzleAttachPoint);

		UParticleSystemComponent* TrailPSC = UGameplayStatics::SpawnEmitterAtLocation(this, TrailFX, Origin);
		if (TrailPSC)
		{
			FString TestHUDString = FString(TEXT("TrailPSC->SetVectorParameter(TrailTargetParam, EndPoint);"));
			TrailPSC->SetVectorParameter(TrailTargetParam, EndPoint);
		}
	}
}

float AFPSWeapon::PlayWeaponAnimation(UAnimMontage* Animation)
{
	float Duration = 0.0f;
	if (MyPawn)
	{
		//UAnimMontage* UseAnim = Animation;

			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Duration = MyPawn->PlayAnimMontage(UseAnim);");
			Duration = MyPawn->PlayAnimMontage(Animation);
		
	}

	return Duration;
}

void AFPSWeapon::StopWeaponAnimation(UAnimMontage* Animation)
{
	if (MyPawn)
	{
		//UAnimMontage* UseAnim = Animation;
		if (Animation)
		{
			MyPawn->StopAnimMontage(Animation);
		}
	}
}


int32 AFPSWeapon::GetCurrentAmmo() const
{
	return CurrentAmmo;
}

int32 AFPSWeapon::GetCurrentAmmoInMagazine() const
{
	return WeaponConfig.CurrentAmmoInMagazine;
}

int32 AFPSWeapon::GetAmmoPerClip() const
{
	return WeaponConfig.AmmoPerMagazine;
}

int32 AFPSWeapon::GetMaxAmmo() const
{
	return WeaponConfig.MaxAmmo;
}

FVector AFPSWeapon::GetMuzzleLocation() const
{
	//USkeletalMeshComponent* UseMesh = GetMesh();
	return WeaponMesh->GetSocketLocation(MuzzleAttachPoint);
}