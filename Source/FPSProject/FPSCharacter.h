// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "FPSWeapon.h"
#include "FPSHUD.h"
#include "FPSCharacter.generated.h"

UCLASS()
class FPSPROJECT_API AFPSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	// Constructor for AFPSCharacter
	AFPSCharacter(const FObjectInitializer& ObjectInitializer);

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	UCameraComponent* FirstPersonCameraComponent;

	// Pawn mesh: 1st person view (arms; seen only by self)
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* FirstPersonMesh;

	// wallhumpbox component
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UBoxComponent* WallhumpBox;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UPawnNoiseEmitterComponent* NoiseEmitterComponent;

	/** weapons in inventory */
	UPROPERTY(Transient)
	TArray<class AFPSWeapon*> Inventory;

	/** default inventory list */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	TArray<TSubclassOf<class AFPSWeapon> > DefaultInventoryClasses;

	/** currently equipped weapon */
	UPROPERTY(Transient)
	class AFPSWeapon* CurrentWeapon;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<UCameraShake> DamageCameraShake;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<UCameraShake> JumpCameraShake;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<UCameraShake> LandCameraShake;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<UCameraShake> UnproneCameraShake;

	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<UCameraShake> RunningCameraShake;

	UPROPERTY(EditDefaultsOnly)
	float MaxHealth;

	UPROPERTY(BlueprintReadOnly)
	float CurrentHealth;

	UFUNCTION()
	float GetCurrentHealth();

	UFUNCTION()
	float GetCurrentStamina();

	UPROPERTY(BlueprintReadOnly)
	int32 Score;

	UFUNCTION()
	void AddScore();

	UFUNCTION(BlueprintCallable, Category = Score)
	int32 GetScore();

	UFUNCTION(BlueprintCallable, Category = Score)
	float GetCurrentSlowMotionTime();

	/** Take damage, handle death */
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;


	// wallrunbox component
	//UPROPERTY(VisibleAnywhere, Category = "Components")
	//class UBoxComponent* WallrunBox;

	//handles moving forward/backward
	UFUNCTION()
	void MoveForward(float Val);

	//handles strafing
	UFUNCTION()
	void MoveRight(float Val);
	
	//sets jump flag when key is pressed
	UFUNCTION()
	void OnStartJump();

	//clears jump flag when key is released
	UFUNCTION()
	void OnStopJump();

	UFUNCTION()
	void OnStartCrouch2();

	UFUNCTION()
	void OnStopCrouch2();

	UFUNCTION()
	void SetCrouching(bool bNewCrouching);

	UFUNCTION()
	bool CanIncreaseCollision(float newWidth, float newHeight, FVector newLoc);

	UFUNCTION()
	void OnStartSprint();

	UFUNCTION()
	void OnStopSprint();

	UFUNCTION()
	void OnStartDiving();

	UFUNCTION()
	void DoDive(FVector LaunchVelocity);

	UFUNCTION()
	void ResetDive();

	UFUNCTION()
	void OnBeginOverlapFront(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnEndOverlapFront(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	//UFUNCTION()
	//bool SphereTraceSingle_NEW(UObject* WorldContextObject, const FVector Start, const FVector End, float Radius, ETraceTypeQuery TraceChannel, bool bTraceComplex, const TArray<AActor*>& ActorsToIgnore, EDrawDebugTrace::Type DrawDebugType, FHitResult& OutHit, bool bIgnoreSelf);

	UFUNCTION(BlueprintCallable, Category = "HUD")
	AFPSWeapon* GetCurrentWeapon() const;

	UFUNCTION(BlueprintCallable, Category = "HUD")
	int32 GetCurrentAmmoInMagazine() const;

	UFUNCTION()
	void SetCurrentWeapon(class AFPSWeapon* NewWeapon, class AFPSWeapon* LastWeapon);

	UFUNCTION()
	void AddWeapon(AFPSWeapon* Weapon);

	UFUNCTION()
	void OnReload();

	UFUNCTION()
	void SlowMotion();

	UFUNCTION()
	void StopSlowMotion();

	UFUNCTION()
	FName GetWeaponAttachPoint() const;

	UFUNCTION()
	USkeletalMeshComponent* GetSpecifcPawnMesh() const;

	UFUNCTION()
	bool IsAlive();

	void OnDeath();
	/*
	UFUNCTION()
	void OnBeginOverlapSides(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnEndOverlapSides(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	*/

	//////////////////////////////////////////////////////////////////////////
	// Weapon Controls

	UFUNCTION()
	void OnFire();

	UFUNCTION()
	void OnStopFire();

	UFUNCTION()
	void OnAltFire();

	UFUNCTION()
	void ChangeFiremode();

	//////////////////////////////////////////////////////////////////////////
	// Animations

	/** play anim montage */
	virtual float PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate = 1.f, FName StartSectionName = NAME_None) override;

	/** stop playing montage */
	virtual void StopAnimMontage(class UAnimMontage* AnimMontage) override;

	/** stop playing all montages */
	void StopAllAnimMontages();

	void ResetTookDamage();

	/*
	AFPSWeapon* CurrentWeapon;

	UPROPERTY(VisibleAnywhere, Category = Spawn)
	TSubclassOf<class AFPSWeapon> WeaponSpawn;
	*/
	UFUNCTION()
	UAudioComponent* PlayCharacterSound(USoundCue* Sound);

	UAudioComponent* AC;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* DeathSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* DamageSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* JumpSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* WallhumpSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* EnterSlowMotionSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* ExitSlowMotionSound;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* SlowMotionSound;

	/** Gun muzzle's offset from the camera location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FVector MuzzleOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AFPSProjectile> ProjectileClass;

	/** Weapon class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AFPSWeapon> WeaponClass;

	/** socket or bone name for attaching weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	FName WeaponAttachPoint;

	UWorld* World;
	FTimerHandle ResetDiveTimerHandle;
	FTimerHandle AllowDiveTimerHandle;
	FTimerHandle WallhumpTimer;
	FTimerHandle DeathTimerHandle;
	FTimerHandle HitNotificationTimerHandle;
	FTimerHandle SlowMotionTimerHandle;

	bool bIsCrouching;

	float CrouchHeight;
	float CrouchWidth;
	float newHeight;
	float newRadius;
	float TotalHeight;
	float TotalWidth;
	float ProneHeight;
	float ProneWidth;

	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	float RunningSpeedModifier;

	UPROPERTY(BlueprintReadOnly)
	float Stamina;

	UPROPERTY(BlueprintReadOnly)
	float SlowMotionTime;

	UPROPERTY(EditDefaultsOnly, Category = Pawn)
	float SlowMotionTimeMAX;

	bool bHasCrouched;
	bool bWantsToRun;
	bool bIsTargeting;
	bool bIsFullStamina;
	bool bCanRun;
	bool bAllowUnProne;
	bool bHitTheGround;
	bool bWantsToUnProne;
	bool bCanDive;	
	bool bHasWallhumped;
	bool bMidAir;

	UPROPERTY(BlueprintReadOnly, Category = Pawn)
	bool bInSlowMotion;

	UPROPERTY(BlueprintReadOnly, Category = Pawn)
	bool bTookDamage;

	UPROPERTY(BlueprintReadOnly)
	bool bIsDying;
	//bool bWantsToWallrun;

	// overlapping bools
	bool bTouchingWallFront;
	//bool bTouchingWall;

	/** current firing state */
	uint8 bWantsToFire : 1;

	uint8 bWallhumpCount;

	UPROPERTY(BlueprintReadOnly, Category = Pawn)
	bool bIsProning;

	UPROPERTY(BlueprintReadOnly, Category = Pawn)
	bool bIsDiving;

	UPROPERTY(BlueprintReadOnly, Category = Pawn)
	bool bIsRunning;

	UFUNCTION(BlueprintCallable, Category = Pawn)
	float GetRunningSpeedModifier();

	UFUNCTION(BlueprintCallable, Category = Pawn)
	bool IsTargeting() const;

	UFUNCTION(BlueprintCallable, Category = Pawn)
	bool IsRunning() const;

	UFUNCTION(BlueprintCallable, Category = Pawn)
	bool IsFullStamina();

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	FRotator GetAimOffsets() const;

	//virtual float TakeDamage(float Damage, class AActor* DamageCauser) override;

	void Death();
};
