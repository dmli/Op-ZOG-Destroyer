// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FPSEnemySpawner.generated.h"

UCLASS()
class FPSPROJECT_API AFPSEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPSEnemySpawner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	int32 Iteration;

	float TimeToSpawn;

	UPROPERTY(EditAnywhere, Category = General)
	TSubclassOf<class AActor> WhatToSpawn;

	UPROPERTY(EditAnywhere, Category = Location)
	FVector SpawnLocation;

	UPROPERTY(EditAnywhere, Category = Location)
	FRotator SpawnRotation;
};
