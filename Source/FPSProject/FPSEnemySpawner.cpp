// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSProject.h"
#include "FPSBot.h"
#include "FPSEnemySpawner.h"


// Sets default values
AFPSEnemySpawner::AFPSEnemySpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//static ConstructorHelpers::FObjectFinder<UClass> BotEnemy(TEXT("Class'/Game/Enemies/FPSBot.FPSBot'"));
	//static ConstructorHelpers::FObjectFinder<UBlueprint> BotEnemy(TEXT("Blueprint'/Game/FPSBot.FPSBot'"));
	//Blueprint'/Game/Blueprints/Enemies/FPSBot.FPSBot'
	//WhatToSpawn = (UClass*)BotEnemy.Object->GeneratedClass;
	//WhatToSpawn = BotEnemy.Object;
}

// Called when the game starts or when spawned
void AFPSEnemySpawner::BeginPlay()
{
	Super::BeginPlay();
	
	Iteration = 1;
	TimeToSpawn = 5.f;
}

// Called every frame
void AFPSEnemySpawner::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (Iteration > 0)
	{
		TimeToSpawn -= DeltaTime;
		if (TimeToSpawn < 0.f)
		{  
			FVector NewLocation = GetActorLocation() + FVector(0.f, 0.f, 300.f);
			//GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Yellow, FString::SanitizeFloat(GetActorLocation().X) + FString::FString(" ") + FString::SanitizeFloat(GetActorLocation().Y) + FString::FString(" ") + FString::SanitizeFloat(GetActorLocation().Z));

			AActor* const SpawningObject = GetWorld()->SpawnActor<AActor>(WhatToSpawn, NewLocation, FRotator::ZeroRotator);
			TimeToSpawn = TimeToSpawn + 15.f - Iteration;
			Iteration++;
			// Housekeeping so that we dont spawn new actors forever  
			//NewActor->Iteration = Iteration - 1;
			//Iteration = 0; // stop ourselves spawning any more  
		}
	}
}

