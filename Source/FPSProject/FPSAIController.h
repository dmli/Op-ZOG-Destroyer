// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "FPSAIController.generated.h"

/**
 * 
 */
UCLASS()
class FPSPROJECT_API AFPSAIController : public AAIController
{
	GENERATED_BODY()
	
	/*Called when the AI Controller possesses a Pawn*/
	virtual void Possess(APawn* InPawn) override;

	AFPSAIController();

	/*This property is used to find a certain key for our blackboard.
	We will create the blackboard later in this tutorial*/
	UPROPERTY(EditDefaultsOnly)
	FName TargetKey = "SensedPawn";

	UPROPERTY(EditDefaultsOnly)
	FName TargetLocationKeyName = "TargetLocation";

	UPROPERTY(EditDefaultsOnly)
	FName TargetStrafeLocationKeyName = "StrafeTargetLocation";
	
protected:
	/*A Behavior tree component in order to be able to call specific functions like starting our BT*/
	UBehaviorTreeComponent* BehaviorTreeComp;

	/*A Blackboard component which will be used to initialize our Blackboard Values*/
	UBlackboardComponent* BlackboardComp;

public:

	/*Sets the new sensed target value inside our Blackboard values*/
	void SetSensedTarget(APawn* NewTarget);

	void SetMoveToTarget(APawn* Pawn);

	virtual void UpdateControlRotation(float DeltaTime, bool bUpdatePawn = true) override;

	void ShootEnemy();

	class AFPSCharacter* GetEnemy() const;

	void FindPointNearEnemy(APawn* Pawn);
};
