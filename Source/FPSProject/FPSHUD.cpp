// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSProject.h"
#include "FPSHUD.h"
#include "FPSCharacter.h"

AFPSHUD::AFPSHUD(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("Texture2D'/Game/xhair4.xhair4'"));
	CrosshairTex = CrosshairTexObj.Object;

	HealthBar = UCanvas::MakeIcon(HUDAssets02Texture, 67, 212, 372, 50);
	HealthBarBg = UCanvas::MakeIcon(HUDAssets02Texture, 67, 162, 372, 50);

	HealthIcon = UCanvas::MakeIcon(HUDAssets02Texture, 78, 262, 28, 28);

	Offset = 20.0f;
	bHasTakenDamage = false;
	LastHitTime = 0.0f;
	HitNotifyDisplayTime = 0.75f;
	LastDamageTaken = 0.0f;
	LastDamageAngle = 0.0f;
	bHUDLeftSide = false;

	/*
	HitNotifyIcon[EFPSHudPosition::Left] = UCanvas::MakeIcon(HitNotifyTexture, 158, 831, 585, 392);
	HitNotifyIcon[EFPSHudPosition::FrontLeft] = UCanvas::MakeIcon(HitNotifyTexture, 369, 434, 460, 378);
	HitNotifyIcon[EFPSHudPosition::Front] = UCanvas::MakeIcon(HitNotifyTexture, 848, 284, 361, 395);
	HitNotifyIcon[EFPSHudPosition::FrontRight] = UCanvas::MakeIcon(HitNotifyTexture, 1212, 397, 427, 394);
	HitNotifyIcon[EFPSHudPosition::Right] = UCanvas::MakeIcon(HitNotifyTexture, 1350, 844, 547, 321);
	HitNotifyIcon[EFPSHudPosition::BackRight] = UCanvas::MakeIcon(HitNotifyTexture, 1232, 1241, 458, 341);
	HitNotifyIcon[EFPSHudPosition::Back] = UCanvas::MakeIcon(HitNotifyTexture, 862, 1384, 353, 408);
	HitNotifyIcon[EFPSHudPosition::BackLeft] = UCanvas::MakeIcon(HitNotifyTexture, 454, 1251, 371, 410);
	*/
}

void AFPSHUD::DrawHUD()
{
	Super::DrawHUD();	

	ScaleUI = Canvas->ClipY / 1080.0f;
	//DrawCrosshair();
	
	// Draw very simple crosshair
	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);
	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition((Center.X - (CrosshairTex->GetSurfaceWidth() * 0.5)),
		(Center.Y - (CrosshairTex->GetSurfaceHeight() * 0.5f)));
	// draw the crosshair
	FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);

	const float CurrentTime = GetWorld()->GetTimeSeconds();

	if (bHasTakenDamage && CurrentTime - LastHitTime < HitNotifyDisplayTime) {
		float AlphaValue = LastDamageTaken * 0.1;
		if (CurrentTime - LastHitTime > HitNotifyDisplayTime * 0.6f) {
			AlphaValue -= 0.15f;
		}		
		Canvas->PopSafeZoneTransform();
		FCanvasTileItem LowHealth(FVector2D(0, 0), LowHealthOverlayTexture->Resource, FVector2D(Canvas->ClipX, Canvas->ClipY), FLinearColor(1.0f, 0.0f, 0.0f, AlphaValue));
		LowHealth.BlendMode = SE_BLEND_Translucent;
		Canvas->DrawItem(LowHealth);
		Canvas->ApplySafeZoneTransform();

		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::SanitizeFloat(LastDamageAngle));
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::FromInt(bHUDLeftSide));
		if (LastDamageAngle < 45.f) {
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Front Damage Indicator");
			FCanvasTileItem HitIndicatorUp(FVector2D(0, 0), HitIndicatorUp->Resource, FVector2D(Canvas->ClipX, Canvas->ClipY), FLinearColor(1.0f, 0.0f, 0.0f, AlphaValue * 0.75f));
			HitIndicatorUp.BlendMode = SE_BLEND_Translucent;
			Canvas->DrawItem(HitIndicatorUp);
		}

		if (LastDamageAngle > 135.f) {
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Back Damage Indicator");
			FCanvasTileItem HitIndicatorDown(FVector2D(0, 0), HitIndicatorDown->Resource, FVector2D(Canvas->ClipX, Canvas->ClipY), FLinearColor(1.0f, 0.0f, 0.0f, AlphaValue * 0.75f));
			HitIndicatorDown.BlendMode = SE_BLEND_Translucent;
			Canvas->DrawItem(HitIndicatorDown);
		}

		if (bHUDLeftSide && LastDamageAngle > 45.f && LastDamageAngle < 135.f) {
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Right Damage Indicator");
			FCanvasTileItem HitIndicatorRight(FVector2D(0, 0), HitIndicatorRight->Resource, FVector2D(Canvas->ClipX, Canvas->ClipY), FLinearColor(1.0f, 0.0f, 0.0f, AlphaValue * 0.75f));
			HitIndicatorRight.BlendMode = SE_BLEND_Translucent;
			Canvas->DrawItem(HitIndicatorRight);
		}

		if (!bHUDLeftSide && LastDamageAngle > 45.f && LastDamageAngle < 135.f) {
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Left Damage Indicator");
			FCanvasTileItem HitIndicatorLeft(FVector2D(0, 0), HitIndicatorLeft->Resource, FVector2D(Canvas->ClipX, Canvas->ClipY), FLinearColor(1.0f, 0.0f, 0.0f, AlphaValue * 0.75f));
			HitIndicatorLeft.BlendMode = SE_BLEND_Translucent;
			Canvas->DrawItem(HitIndicatorLeft);
		}
	}
	else if (bHasTakenDamage && CurrentTime - LastHitTime > HitNotifyDisplayTime) {
		bHasTakenDamage = false;
	}
}

void AFPSHUD::NotifyHitAngle(float Angle, bool bLeftSide, float Damage)
{
	LastHitTime = GetWorld()->GetTimeSeconds();
	bHasTakenDamage = true;
	LastDamageTaken = Damage;
	LastDamageAngle = FMath::RadiansToDegrees(acos(Angle));
	bHUDLeftSide = bLeftSide;
}
/*
void AFPSHUD::DrawHitIndicator()
{
	const float CurrentTime = GetWorld()->GetTimeSeconds();
	if (CurrentTime - LastHitTime <= HitNotifyDisplayTime)
	{
		const float StartX = Canvas->ClipX / 2.0f;
		const float StartY = Canvas->ClipY / 2.0f;

		for (uint8 i = 0; i < 8; i++)
		{
			const float TimeModifier = FMath::Max(0.0f, 1 / HitNotifyDisplayTime);
			const float Alpha = TimeModifier;
			Canvas->SetDrawColor(255, 255, 255, FMath::Clamp(FMath::TruncToInt(Alpha * 255 * 1.5f), 0, 255));
			Canvas->DrawIcon(HitNotifyIcon[i],
				StartX + (HitNotifyIcon[i].U - HitNotifyTexture->GetSizeX() / 2 + Offsets[i].X) * ScaleUI,
				StartY + (HitNotifyIcon[i].V - HitNotifyTexture->GetSizeY() / 2 + Offsets[i].Y) * ScaleUI,
				ScaleUI);
		}
	}
}*/

float AFPSHUD::GetHealth()
{
	AFPSCharacter* MyPawn = Cast<AFPSCharacter>(GetOwningPawn());
	//FString message = TEXT("CurrentHealth ") + FString::SanitizeFloat(MyPawn->CurrentHealth);
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);
	const float Health = MyPawn->GetCurrentHealth();
	return Health;
}

float AFPSHUD::GetStamina()
{
	AFPSCharacter* MyPawn = Cast<AFPSCharacter>(GetOwningPawn());
	//FString message = TEXT("CurrentHealth ") + FString::SanitizeFloat(MyPawn->CurrentHealth);
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);
	const float Stamina = MyPawn->GetCurrentStamina();
	return Stamina;
}



