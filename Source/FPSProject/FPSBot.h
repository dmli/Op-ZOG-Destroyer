// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Perception/PawnSensingComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "FPSCharacter.h"
#include "FPSAIController.h"
#include "FPSWeapon.h"
#include "FPSImpactEffects.h"
#include "GameFramework/Actor.h"
#include "FPSBot.generated.h"

USTRUCT()
struct FBotWeaponData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, Category = Ammo)
	int32 AmmoPerMagazine;

	UPROPERTY(VisibleAnywhere, Category = Ammo)
	int32 CurrentAmmoInMagazine;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float TimeBetweenShots;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float WeaponSpread;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float WeaponDamage;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float WeaponRange;

	/** type of damage */
	UPROPERTY(EditDefaultsOnly, Category = WeaponStat)
	TSubclassOf<UDamageType> DamageType;
};

UCLASS()
class FPSPROJECT_API AFPSBot : public ACharacter
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FBotWeaponData BotWeaponConfig;

	// Sets default values for this character's properties
	AFPSBot(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Awareness)
	UPawnSensingComponent* PawnSensingComp;

	UFUNCTION()
	void OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume);

	UFUNCTION()
	void OnSeePlayer(APawn *OtherPawn);

	UPROPERTY(EditDefaultsOnly)
	UBehaviorTree* BehaviorTree;
	
	UPROPERTY(EditDefaultsOnly, Category = Animation)
	UAnimMontage* DeathAnim;

	UPROPERTY(EditDefaultsOnly)
	class AFPSAIController* AIController;

	UPROPERTY(EditDefaultsOnly)
	class AFPSCharacter* SensedPawn;

	virtual void FaceRotation(FRotator NewRotation, float DeltaTime = 0.f) override;

	/** currently equipped weapon */
	UPROPERTY(Transient)
	class AFPSWeapon* CurrentWeapon;

	/** socket or bone name for attaching weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	FName WeaponAttachPoint;

	/** Weapon class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AFPSWeapon> WeaponClass;

	/** weapons in inventory */
	UPROPERTY(Transient)
	TArray<class AFPSWeapon*> Inventory;

	/** default inventory list */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
	TArray<TSubclassOf<class AFPSWeapon> > DefaultInventoryClasses;

	UFUNCTION()
	void AddWeapon(AFPSWeapon* Weapon);

	UFUNCTION()
	AFPSWeapon* GetCurrentWeapon() const;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Config)
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	float MaxHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Config)
	float CurrentHealth;

	UFUNCTION()
	void OnFire();

	UFUNCTION()
	void StartSlowMotion(float SlowMotionTime);

	UFUNCTION()
	void StopSlowMotion();

	bool bInSlowMotion;

	float SlowMotionTimer;

	void SpawnImpactEffects(const FHitResult& Impact);

	void SpawnTrailEffect(const FVector& EndPoint);

	/** impact effects */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	TSubclassOf<class AFPSImpactEffects> ImpactTemplate;

	/** smoke trail */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UParticleSystem* TrailFX;

	/** param name for beam target in smoke trail */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName TrailTargetParam;

	/** name of bone/socket for muzzle in weapon mesh */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	FName MuzzleAttachPoint;

	/** FX for muzzle flash */
	UPROPERTY(EditDefaultsOnly, Category = Effects)
	UParticleSystem* MuzzleFX;

	/** spawned component for muzzle FX */
	UPROPERTY(Transient)
	UParticleSystemComponent* MuzzlePSC;

	/** firing audio (bLoopedFireSound set) */
	UPROPERTY(Transient)
	UAudioComponent* FireAC;

	/** single fire sound (bLoopedFireSound not set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* FireSound;

	/** looped fire sound (bLoopedFireSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* FireLoopSound;

	/** finished burst sound (bLoopedFireSound set) */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* FireFinishSound;

	/** out of ammo sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* OutOfAmmoSound;

	/** reload sound */
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundCue* ReloadSound;

	UAudioComponent* PlayWeaponSound(USoundCue* Sound);

	class AFPSBot* GetPawnOwner() const;

	/** pawn owner */
	UPROPERTY(Transient)
	class AFPSBot* MyPawn;

	FHitResult WeaponTrace(const FVector &TraceFrom, const FVector &TraceTo) const;

	void ProcessHit_InstantHit(const FHitResult &Impact, const FVector &Origin, const FVector &ShootDir, int32 RandomSeed, float ReticleSpread);

	void DealDamage(const FHitResult& Impact, const FVector& ShootDir);

	/** Take damage, handle death */
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	void HitBone(FName BoneName, FVector ImpactPoint);

	FName HitBoneName;

	FVector ImpactPointVector;

	void OnDeath();

	bool bIsDying;

	virtual float PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate = 1.f, FName StartSectionName = NAME_None) override;

	void GetPreviousActorRotation();

	FRotator PrevAngle;

	float AngleDifference;

	void StopAllAnimMontages();

	FTimerHandle DeathTimerHandle;
	FTimerHandle DeathTimerHandle2;
	FTimerHandle LastShotTimerHandle;
	FTimerHandle StopFireTimerHandle;
	FTimerHandle ReactionTimeTimerHandle;
	FTimerHandle RotationTimeTimerHandle;

	bool bCloseRange;
	bool bLongRange;
	bool bMidRange;

	int BurstCounter;

	void CanFire();

	bool bCanFire;

	void StopFire();

	void AfterDeath();

	void SetRagdoll();

	//virtual bool Die(float KillingDamage, struct FDamageEvent const& DamageEvent, class AController* Killer, class AActor* DamageCauser) override;
};
