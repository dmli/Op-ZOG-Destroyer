// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSProject.h"
#include "FPSBot.h"


// Sets default values
AFPSBot::AFPSBot(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComp = ObjectInitializer.CreateDefaultSubobject<UPawnSensingComponent>(this, TEXT("Pawn Sensor"));
	PawnSensingComp->SensingInterval = .25f; // 4 times per second
	PawnSensingComp->bOnlySensePlayers = false;
	PawnSensingComp->SetPeripheralVisionAngle(80.0f);
	PawnSensingComp->SightRadius = 3000;
	PawnSensingComp->HearingThreshold = 6400;
	PawnSensingComp->LOSHearingThreshold = 128000;

	BehaviorTree = CreateDefaultSubobject<UBehaviorTree>(TEXT("BehaviorTreeReference"));

	GetMesh()->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::AlwaysTickPoseAndRefreshBones;
	GetMesh()->CastShadow = true;
	GetMesh()->SetCollisionObjectType(ECC_Pawn);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	GetMesh()->SetCollisionResponseToAllChannels(ECR_Block);
	GetMesh()->SetCollisionProfileName(TEXT("CharacterMesh"));

	/* Ignore this channel or it will absorb the trace impacts instead of the skeletal mesh */
	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	GetCapsuleComponent()->SetCapsuleHalfHeight(96.0f, false);
	GetCapsuleComponent()->SetCapsuleRadius(42.0f);

	/* These values are matched up to the CapsuleComponent above and are used to find navigation paths */
	GetMovementComponent()->NavAgentProps.AgentRadius = 42;
	GetMovementComponent()->NavAgentProps.AgentHeight = 192;

	WeaponMesh = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("WeaponMesh"));
	WeaponMesh->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::AlwaysTickPoseAndRefreshBones;
	WeaponMesh->bReceivesDecals = false;
	WeaponMesh->CastShadow = true;
	WeaponMesh->SetCollisionObjectType(ECC_WorldDynamic);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	WeaponMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	WeaponMesh->AttachTo(GetMesh(), WeaponAttachPoint);

	bIsDying = false;
	bCanFire = true;
	bCloseRange = false;
	bInSlowMotion = false;
	BurstCounter = 0;
	AngleDifference = 0.0f;
	SlowMotionTimer = 0.0f;

	ImpactPointVector = FVector::ZeroVector;
}

// Called when the game starts or when spawned
void AFPSBot::BeginPlay()
{
	Super::BeginPlay();
	
	if (PawnSensingComp)
	{
		PawnSensingComp->OnSeePawn.AddDynamic(this, &AFPSBot::OnSeePlayer);
		PawnSensingComp->OnHearNoise.AddDynamic(this, &AFPSBot::OnHearNoise);
	}

	/*
	int32 NumWeaponClasses = DefaultInventoryClasses.Num();
	for (int32 i = 0; i < NumWeaponClasses; i++)
	{
		if (DefaultInventoryClasses[i])
		{
			FActorSpawnParameters SpawnInfo;
			//SpawnInfo.bNoCollisionFail = true;
			AFPSWeapon* NewWeapon = GetWorld()->SpawnActor<AFPSWeapon>(DefaultInventoryClasses[i], SpawnInfo);
			AddWeapon(NewWeapon);
		}
	}*/

	//GetCurrentWeapon();
	//CurrentWeapon->SetOwningBot(this);

	WeaponMesh->AttachTo(GetMesh(), WeaponAttachPoint);

	CurrentHealth = MaxHealth;
}

// Called every frame
void AFPSBot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bInSlowMotion)
	{
		SlowMotionTimer -= (DeltaTime * 2.5f);
		if (SlowMotionTimer <= 0)
		{
			SlowMotionTimer = 0.0f;
			StopSlowMotion();
		}
	}

	if (bLongRange || bMidRange || bCloseRange)
	{
		//FVector TargetLoc = SensedPawn->GetActorLocation();
		//FVector SelfLoc = GetActorLocation();
		//float Angle = SelfLoc.CosineAngle2D(TargetLoc);

		FRotator Angle = GetActorRotation();
		GetWorld()->GetTimerManager().SetTimer(RotationTimeTimerHandle, this, &AFPSBot::GetPreviousActorRotation, 0.250f);

		if (!bIsDying && bCanFire)
		{
			Angle.Clamp();
			PrevAngle.Clamp();

			AngleDifference = Angle.Pitch - PrevAngle.Pitch;
			if (AngleDifference < 0)
			{
				AngleDifference *= -1.0f;
			}
			AngleDifference = Angle.Roll - PrevAngle.Roll;
			if (AngleDifference < 0)
			{
				AngleDifference *= -1.0f;
			}
			AngleDifference = Angle.Yaw - PrevAngle.Yaw;
			if (AngleDifference < 0)
			{
				AngleDifference *= -1.0f;
			}

			float Delay = FMath::FRandRange(0.15f, 0.5f);
			//GetWorld()->GetTimerManager().SetTimer(ReactionTimeTimerHandle, this, &AFPSBot::OnFire, Delay);
			OnFire();
		}
	}
}

// Called to bind functionality to input
void AFPSBot::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void AFPSBot::OnSeePlayer(APawn *OtherPawn)
{
	//FString message = TEXT("Saw Actor ") + OtherPawn->GetName();
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);
	AFPSCharacter* SensedPawn = Cast<AFPSCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	AFPSAIController* AIController = Cast<AFPSAIController>(GetController());
	//AFPSCharacter* SensedPawn = Cast<AFPSCharacter>(OtherPawn);
	if (AIController)
	{
		AIController->SetSensedTarget(OtherPawn);
		AIController->SetMoveToTarget(OtherPawn);
		AIController->FindPointNearEnemy(OtherPawn);
		if (float x = FMath::FRandRange(0.0f, 1.0f) > 0.5f)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("AIController->FindPointNearEnemy(OtherPawn);"));
			//AIController->FindPointNearEnemy(OtherPawn);
		}
	}

	FVector TargetLoc = OtherPawn->GetActorLocation();
	FVector SelfLoc = GetActorLocation();
	float Distance = FVector::Dist(SelfLoc, TargetLoc);

	//const FString Distance = FString::Printf(TEXT(" at distance %f"), FVector::Dist(SelfLoc, TargetLoc));
	//FString message = TEXT("I am") + Distance + TEXT(" from the player.");
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);

	if (Distance < 900.f)
	{
		bCloseRange = true;
		bMidRange = false;
		bLongRange = false;
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Close range.");
	}

	if (Distance > 900.f && Distance < 2000.f)
	{
		bCloseRange = false;
		bMidRange = true;
		bLongRange = false;
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Mid range.");
	}

	if (Distance > 2000.f)
	{
		bCloseRange = false;
		bMidRange = false;
		bLongRange = true;
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Long range.");
	}
}

void AFPSBot::OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume)
{
	//const FString VolumeDesc = FString::Printf(TEXT(" at volume %f"), Volume);
	//FString message = TEXT("Heard Actor ") + OtherActor->GetName() + VolumeDesc;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);

	AFPSAIController* AIController = Cast<AFPSAIController>(GetController());
	if (AIController)
	{
		AIController->SetSensedTarget(OtherActor);
		AIController->SetMoveToTarget(OtherActor);
	}
}

void AFPSBot::StartSlowMotion(float SlowMotionTime)
{
	bInSlowMotion = true;
	CustomTimeDilation = 0.4f;
	SlowMotionTimer = SlowMotionTime;
	//CurrentWeapon->StartSlowMotion();
}

void AFPSBot::StopSlowMotion()
{
	//CurrentWeapon->StopSlowMotion();
	bInSlowMotion = false;
	CustomTimeDilation = 1.0f;
}

void AFPSBot::FaceRotation(FRotator NewRotation, float DeltaTime)
{
	FRotator CurrentRotation = FMath::RInterpTo(GetActorRotation(), NewRotation, DeltaTime, 8.0f);

	Super::FaceRotation(CurrentRotation, DeltaTime);

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("AFPSBot::FaceRotation"));
}

void AFPSBot::GetPreviousActorRotation()
{
	PrevAngle = GetActorRotation();
}

void AFPSBot::AddWeapon(AFPSWeapon* Weapon)
{
	if (Weapon && Role == ROLE_Authority)
	{
		//Weapon->OnEnterInventory(this);
		Inventory.AddUnique(Weapon);
		CurrentWeapon = Weapon;
	}
}

void AFPSBot::OnFire()
{
	/*if (!bCanFire)
	{
		GetWorld()->GetTimerManager().SetTimer(StopFireTimerHandle, this, &AFPSBot::StopFire, 0.05f);
	}*/

	AFPSAIController* AIController = Cast<AFPSAIController>(GetController());
	if (AIController == NULL)
	{
		return;
	}

	if (bCanFire && AIController->LineOfSightTo(AIController->GetEnemy(), FVector(0.0f, 0.0f, 0.0f), false))
	{
		float Modifier = bLongRange ? 0.80f : 0.14f;
		float TimerDelay = BotWeaponConfig.TimeBetweenShots * Modifier;
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "TimerDelay = " + FString::SanitizeFloat(TimerDelay));
		/*
		if (bMidRange)
		{
			BurstCounter++;
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "BurstCounter++;" + FString::FromInt(BurstCounter));
		}
		if (bCloseRange)
		{
			BurstCounter++;
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "BurstCounter++;" + FString::FromInt(BurstCounter));
		}
		if (BurstCounter < 5)
		{
			GetWorld()->GetTimerManager().SetTimer(LastShotTimerHandle, this, &AFPSBot::CanFire, TimerDelay);
		}	*/	

		WeaponMesh->GetSocketLocation(MuzzleAttachPoint);
		MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, WeaponMesh, MuzzleAttachPoint);
		MuzzlePSC->ActivateSystem(true);

		PlayWeaponSound(FireSound);

		//const FVector Origin = WeaponMesh->GetSocketLocation(MuzzleAttachPoint);

		const int32 RandomSeed = FMath::Rand();
		FRandomStream WeaponRandomStream(RandomSeed);
		float SpreadMod;
		if (bCloseRange)
		{
			SpreadMod = 8.0f;
			BurstCounter++;
		}
		if (bMidRange)
		{
			SpreadMod = 5.0f;
			BurstCounter++;
		}
		if (bLongRange)
		{
			SpreadMod = 1.0f;
		}
		if (BurstCounter < 5)
		{
			GetWorld()->GetTimerManager().SetTimer(LastShotTimerHandle, this, &AFPSBot::CanFire, TimerDelay);
		}
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::SanitizeFloat(AngleDifference));
		//const float SpreadMod = bLongRange ? 1.0f : 3.0f;
		const float CurrentSpread = BotWeaponConfig.WeaponSpread * AngleDifference;
		const float SpreadCone = FMath::DegreesToRadians(BotWeaponConfig.WeaponSpread * 0.5f);

		//GetActorEyesViewPoint(GetActorLocation(), GetActorRotation());

		const FVector AimDir = GetControlRotation().Vector();
		const FVector StartTrace = WeaponMesh->GetSocketLocation(MuzzleAttachPoint);
		const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, SpreadCone, SpreadCone);
		const FVector EndTrace = StartTrace + ShootDir * BotWeaponConfig.WeaponRange;
		const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);

		ProcessHit_InstantHit(Impact, StartTrace, ShootDir, RandomSeed, CurrentSpread);

		bCanFire = false;

		GetWorld()->GetTimerManager().SetTimer(StopFireTimerHandle, this, &AFPSBot::StopFire, 0.05f);

		if (BurstCounter == 5)
		{
			bCanFire = false;
			GetWorld()->GetTimerManager().SetTimer(LastShotTimerHandle, this, &AFPSBot::CanFire, 1.75f);
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Burst finished.");
		}
	}
}

void AFPSBot::CanFire()
{
	bCanFire = true;
	if (BurstCounter == 5)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "BurstCounter = 0");
		BurstCounter = 0;
	}
}

void AFPSBot::StopFire()
{
	//bWantsToFire = false;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "StopFiring();");

	if (MuzzlePSC != NULL)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "MuzzlePSC->DeactivateSystem();");
		MuzzlePSC->DeactivateSystem();
		MuzzlePSC = NULL;
	}


	/*
	if (bLoopedFireAnim && bPlayingFireAnim)
	{
	StopWeaponAnimation(FireAnim);
	bPlayingFireAnim = false;
	}*/

	if (FireAC)
	{
		FireAC->FadeOut(0.1f, 0.0f);
		FireAC = NULL;

		PlayWeaponSound(FireFinishSound);
	}

	//StopWeaponAnimation(FireAnim);
}

void AFPSBot::ProcessHit_InstantHit(const FHitResult &Impact, const FVector &Origin, const FVector &ShootDir, int32 RandomSeed, float ReticleSpread)
{
	const FVector EndTrace = Origin + ShootDir * BotWeaponConfig.WeaponRange;
	const FVector EndPoint = Impact.GetActor() ? Impact.ImpactPoint : EndTrace;

	//DrawDebugLine(this->GetWorld(), Origin, Impact.TraceEnd, FColor::Red, true, 10000, 10.f);

	if (Impact.GetActor())
	{
		//FString message = TEXT("Hit Actor ") + Impact.GetActor()->GetName();
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);
		//SpawnImpactEffects(Impact);
		DealDamage(Impact, ShootDir);
	}
	else if (Impact.GetActor() == NULL)
	{
		if (Impact.bBlockingHit)
		{
			SpawnImpactEffects(Impact);
			SpawnTrailEffect(Impact.ImpactPoint);
		}
		else
		{
			SpawnTrailEffect(EndTrace);
		}
	}
}

void AFPSBot::DealDamage(const FHitResult& Impact, const FVector& ShootDir)
{
	FPointDamageEvent PointDmg;
	PointDmg.DamageTypeClass = BotWeaponConfig.DamageType;
	PointDmg.HitInfo = Impact;
	PointDmg.ShotDirection = ShootDir;
	PointDmg.Damage = BotWeaponConfig.WeaponDamage;

	Impact.GetActor()->TakeDamage(PointDmg.Damage, PointDmg, AIController, this);
}

FHitResult AFPSBot::WeaponTrace(const FVector &TraceFrom, const FVector &TraceTo) const
{
	static FName WeaponFireTag = FName(TEXT("BotWeaponTrace"));

	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;
	//TraceParams.AddIgnoredActor(this);

	FHitResult Hit(ForceInit);

	GetWorld()->LineTraceSingleByChannel(Hit, TraceFrom, TraceTo, TRACE_WEAPON, TraceParams);

	return Hit;
}

float AFPSBot::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	CurrentHealth -= Damage;

	//FString message = TEXT("My health is: ") + FString::SanitizeFloat(CurrentHealth);
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, message);

	if (CurrentHealth <= 0 && !bIsDying)
	{
		bIsDying = true;
		OnDeath();
		StopFire();
		StopAllAnimMontages();

		FVector ShooterLocation = DamageCauser->GetActorLocation();
		FVector BotLocation = GetActorLocation();
		
		GetMesh()->AddImpulseAtLocation(((ShooterLocation + BotLocation).GetClampedToSize(1.0f, 1.0f) * 15000.f), ImpactPointVector, HitBoneName);
	}

	return Damage;
	/*AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(Controller);
	if (MyPC && MyPC->HasGodMode())
	{
		return 0.f;
	}

	if (Health <= 0.f)
	{
		return 0.f;
	}

	// Modify based on game rules.
	AShooterGameMode* const Game = GetWorld()->GetAuthGameMode<AShooterGameMode>();
	Damage = Game ? Game->ModifyDamage(Damage, this, DamageEvent, EventInstigator, DamageCauser) : 0.f;

	const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.f)
	{
		Health -= ActualDamage;
		if (Health <= 0)
		{
			Die(ActualDamage, DamageEvent, EventInstigator, DamageCauser);
		}
		else
		{
			PlayHit(ActualDamage, DamageEvent, EventInstigator ? EventInstigator->GetPawn() : NULL, DamageCauser);
		}

		MakeNoise(1.0f, EventInstigator ? EventInstigator->GetPawn() : this);
	}

	return ActualDamage;*/
}

void AFPSBot::HitBone(FName BoneName, FVector ImpactPoint)
{
	HitBoneName = BoneName;
	ImpactPointVector = ImpactPoint;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Bone hit: ") + HitBoneName.ToString());
}

void AFPSBot::OnDeath()
{
	bTearOff = true;

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);

	if (GetMesh())
	{
		static FName CollisionProfileName(TEXT("Ragdoll"));
		GetMesh()->SetCollisionProfileName(CollisionProfileName);
	}
	SetActorEnableCollision(true);

	
	GetMesh()->SetAllBodiesSimulatePhysics(true);
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->WakeAllRigidBodies();
	GetMesh()->bBlendPhysics = true;

	
	/*WeaponMesh->DetachFromParent();
	WeaponMesh->SetAllBodiesSimulatePhysics(true);
	WeaponMesh->SetSimulatePhysics(true);
	WeaponMesh->WakeAllRigidBodies();
	WeaponMesh->bBlendPhysics = true;
	WeaponMesh->SetCollisionObjectType(ECC_WorldStatic);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics); 
	WeaponMesh->SetCollisionResponseToAllChannels(ECR_Block);*/

	GetCharacterMovement()->StopMovementImmediately();
	GetCharacterMovement()->DisableMovement();
	GetCharacterMovement()->SetComponentTickEnabled(false);

	//FName HitBone = SensedPawn->CurrentWeapon->GetHitBoneName();
	//GetMesh()->AddImpulseAtLocation((GetActorLocation() + SensedPawn->GetActorLocation() * 10.f), SensedPawn->GetActorLocation(), HitBone);

	//float DeathAnimDuration = PlayAnimMontage(DeathAnim);
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Dead!");
	//SetActorEnableCollision(false);

	GetWorld()->GetTimerManager().SetTimer(DeathTimerHandle, this, &AFPSBot::AfterDeath, 6.0f);
	//GetWorld()->GetTimerManager().SetTimer(DeathTimerHandle2, this, &AFPSBot::SetRagdoll, FMath::Min(0.1f, DeathAnimDuration));

	AFPSCharacter* SensedPawn = Cast<AFPSCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	if (SensedPawn)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("SensedPawn->AddScore();"));
		SensedPawn->AddScore();
	}
}

void AFPSBot::SetRagdoll()
{	
	GetMesh()->SetAllBodiesSimulatePhysics(true);
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->WakeAllRigidBodies();
	GetMesh()->bBlendPhysics = true;
	GetMesh()->SetCollisionProfileName(TEXT("CharacterMesh"));
	//SetActorEnableCollision(false);

	//TurnOff();
	//SetActorHiddenInGame(true);
	//SetLifeSpan(1.0f);
}

void AFPSBot::AfterDeath()
{
	Destroy();
}

float AFPSBot::PlayAnimMontage(class UAnimMontage* AnimMontage, float InPlayRate, FName StartSectionName)
{
	USkeletalMeshComponent* UseMesh = GetMesh();
	if (AnimMontage && UseMesh && UseMesh->AnimScriptInstance)
	{
		return UseMesh->AnimScriptInstance->Montage_Play(AnimMontage, InPlayRate);
	}

	return 0.0f;
}

/*
bool Die(float KillingDamage, struct FDamageEvent const& DamageEvent, class AController* Killer, class AActor* DamageCauser)
{
	// cannot use IsLocallyControlled here, because even local client's controller may be NULL here

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, "Dead.");

	/*
	if (GetNetMode() != NM_DedicatedServer && DeathSound && Mesh1P && Mesh1P->IsVisible())
	{
		UGameplayStatics::PlaySoundAtLocation(this, DeathSound, GetActorLocation());
	}

	Destroy();

	DetachFromControllerPendingDestroy();
	StopAllAnimMontages();

	if (LowHealthWarningPlayer && LowHealthWarningPlayer->IsPlaying())
	{
		LowHealthWarningPlayer->Stop();
	}

	if (RunLoopAC)
	{
		RunLoopAC->Stop();
	}

	// disable collisions on capsule
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);

	if (GetMesh())
	{
		static FName CollisionProfileName(TEXT("Ragdoll"));
		GetMesh()->SetCollisionProfileName(CollisionProfileName);
	}
	SetActorEnableCollision(true);

	// Death anim
	float DeathAnimDuration = PlayAnimMontage(DeathAnim);

	// Ragdoll
	if (DeathAnimDuration > 0.f)
	{
		GetWorldTimerManager().SetTimer(this, &AShooterCharacter::SetRagdollPhysics, FMath::Min(0.1f, DeathAnimDuration), false);
	}
	else
	{
		SetRagdollPhysics();
	}*/

	//return true;
//}

UAudioComponent* AFPSBot::PlayWeaponSound(USoundCue* Sound)
{
	//UAudioComponent* AC = NULL;
	//AFPSBot* MyPawn = GetPawnOwner();

	if (Sound)
	{
		//AC = UGameplayStatics::SpawnSoundAttached(Sound, GetMesh(), WeaponAttachPoint, GetActorLocation(), EAttachLocation::SnapToTarget);
		UGameplayStatics::SpawnSoundAttached(Sound, RootComponent, NAME_None, FVector::ZeroVector, EAttachLocation::SnapToTarget, true);
		FString TestHUDString = FString(TEXT("PlayWeaponSound"));
	}

	return nullptr;
}

void AFPSBot::SpawnImpactEffects(const FHitResult& Impact)
{
	if (ImpactTemplate && Impact.bBlockingHit)
	{
		FHitResult UseImpact = Impact;

		// trace again to find component lost during replication
		if (!Impact.Component.IsValid())
		{
			const FVector StartTrace = Impact.ImpactPoint + Impact.ImpactNormal * 10.0f;
			const FVector EndTrace = Impact.ImpactPoint - Impact.ImpactNormal * 10.0f;
			FHitResult Hit = WeaponTrace(StartTrace, EndTrace);
			UseImpact = Hit;
		}

		AFPSImpactEffects* EffectActor = GetWorld()->SpawnActorDeferred<AFPSImpactEffects>(ImpactTemplate, Impact.ImpactPoint, Impact.ImpactNormal.Rotation());
		if (EffectActor)
		{
			FString TestHUDString = FString(TEXT("EffectActor->SurfaceHit = UseImpact;"));
			EffectActor->SurfaceHit = UseImpact;
			UGameplayStatics::FinishSpawningActor(EffectActor, FTransform(Impact.ImpactNormal.Rotation(), Impact.ImpactPoint));
		}
	}
}

void AFPSBot::SpawnTrailEffect(const FVector& EndPoint)
{
	if (TrailFX)
	{
		const FVector Origin = WeaponMesh->GetSocketLocation(MuzzleAttachPoint);

		UParticleSystemComponent* TrailPSC = UGameplayStatics::SpawnEmitterAtLocation(this, TrailFX, Origin);
		if (TrailPSC)
		{
			FString TestHUDString = FString(TEXT("TrailPSC->SetVectorParameter(TrailTargetParam, EndPoint);"));
			TrailPSC->SetVectorParameter(TrailTargetParam, EndPoint);
		}
	}
}

void AFPSBot::StopAllAnimMontages()
{
	if (GetMesh() && GetMesh()->AnimScriptInstance)
	{
		GetMesh()->AnimScriptInstance->Montage_Stop(0.0f);
	}
}

AFPSWeapon* AFPSBot::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

class AFPSBot* AFPSBot::GetPawnOwner() const
{
	return MyPawn;
}

